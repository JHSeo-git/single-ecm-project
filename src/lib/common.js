export const pressedKey = (fn) => (e) => {
  if (e.key === "Enter") {
    fn();
  }
};
