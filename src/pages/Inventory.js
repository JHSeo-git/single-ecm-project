import React, { useState } from "react";

import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";

import SearchSPInventory from "components/search/SearchSPInventory";
import SearchSPInventoryForm from "components/search/SearchSPInventoryForm";
import SearchSPInventoryDetailB from "components/search/SearchSPInventoryDetailB";

const Inventory = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };

  const [spId, setSpId] = useState("");

  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };
  const onDetailClick = (spIdValue) => {
    setSpId(spIdValue);
    setMode("INVENTORY_DETAIL");
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <SearchSPInventory
          onSearchClick={onSearchClick}
          onDetailClick={onDetailClick}
        />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <SearchSPInventoryForm onClose={onClose} />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="INVENTORY_DETAIL">
        <SearchSPInventoryDetailB spId={spId} onClose={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default Inventory;
