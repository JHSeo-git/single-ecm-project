import React, { useState } from "react";
import DeliveryLanding from "components/delivery/DeliveryLanding";
import DeliverySearchForm from "components/delivery/DeliverySearchForm";

import ModalFullPageAni from "components/common/ModalFullPageAni";

const Delivery = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };
  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <DeliveryLanding onSearchClick={onSearchClick} />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <DeliverySearchForm onBackClick={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default Delivery;
