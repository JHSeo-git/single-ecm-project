import React, { useState } from "react";
import ReceiptLanding from "components/receipt/ReceiptLanding";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import ReceiptSearchForm from "components/receipt/ReceiptSearchForm/ReceiptSearchForm";

const Receipt = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };

  const onSearchClick = () => {
    setMode("SEARCH_FORM");
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <ReceiptLanding onSearchClick={onSearchClick}></ReceiptLanding>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <ReceiptSearchForm onBackClick={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default Receipt;
