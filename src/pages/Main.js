import React from "react";

import MainPage from "components/main/Main";

const Main = () => {
  return <MainPage />;
};

export default Main;
