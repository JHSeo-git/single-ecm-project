import React, { useState } from "react";

import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import Loader from "components/common/Loader";

import WorkOrderList from "components/workOrder/WorkOrderList";
import WorkOrderListSearchForm from "components/workOrder/WorkOrderListSearchForm";
import WorkOrderDetail from "components/workOrder/WorkOrderDetail";

const WorkOrder = () => {
  const [loading, setLoading] = useState(true);
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };

  const [woNo, setWoNo] = useState("");
  const [woType, setWoType] = useState("");
  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };
  const onDetailClick = (woNoValue, woTypeIn) => {
    setWoNo(woNoValue);
    setWoType(woTypeIn);
    setMode("WORK_ORDER_DETAIL");
  };

  return (
    <>
      {/* {loading && <Loader />} */}
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <WorkOrderList
          onSearchClick={onSearchClick}
          onDetailClick={onDetailClick}
        />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <WorkOrderListSearchForm onClose={onClose} />
      </ModalFullPageAni>

      <ModalFullPageAni modeState={mode} pageName="WORK_ORDER_DETAIL">
        <WorkOrderDetail woNo={woNo} woType={woType} onClose={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default WorkOrder;
