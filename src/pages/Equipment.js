import React, { useState } from "react";

import ModalFullPage from "components/common/ModalFullPage";
import ModalFullPageAni from "components/common/ModalFullPageAni";

import SearchEquipment from "components/search/SearchEquipment";
import SearchEquipmentForm from "components/search/SearchEquipmentForm";

const Equipment = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };

  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <SearchEquipment onSearchClick={onSearchClick} />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <SearchEquipmentForm onClose={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default Equipment;
