import React, { useState } from "react";

import ModalFullPage from "components/common/ModalFullPage";

import ModalFullPageAni from "components/common/ModalFullPageAni";

import SearchSP from "components/search/SearchSP";
import SearchSPForm from "components/search/SearchSPForm";

const SpareParts = () => {
  const [mode, setMode] = useState("init");
  const onClose = () => {
    setMode("");
  };

  const onSearchClick = () => {
    // Search Form Render
    setMode("SEARCH_FORM");
  };

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <SearchSP onSearchClick={onSearchClick} />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="SEARCH_FORM">
        <SearchSPForm onClose={onClose} />
      </ModalFullPageAni>
    </>
  );
};

export default SpareParts;
