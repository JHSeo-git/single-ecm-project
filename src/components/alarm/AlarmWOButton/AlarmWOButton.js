import React, { useEffect, useState } from "react";
import cx from "classnames";

import IconBase from "components/common/IconBase";
import { IconPlus } from "components/common/Icons";

import "./AlarmWOButton.scss";

const ToolTip = ({ content, timer = 3000 }) => {
  const [view, setView] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setView(false);
    }, timer);
  }, [timer]);
  return (
    view && (
      <div className="alarm-tool-tip-wrapper">
        <div className="tool-tip">{content}</div>
      </div>
    )
  );
};

const AlarmWOButton = ({ woNo, onClick, viewTooltip }) => {
  return (
    <button className={cx("alarm-wo-button")} disabled={woNo} onClick={onClick}>
      {woNo ? (
        <span className="wo-text">{woNo}</span>
      ) : (
        <div className="new-wo">
          <IconBase>
            <IconPlus />
          </IconBase>
          <span className="wo-text">WO</span>
        </div>
      )}
      {viewTooltip && <ToolTip content="버튼을 누르면 WO가 생성됩니다" />}
    </button>
  );
};

export default AlarmWOButton;
