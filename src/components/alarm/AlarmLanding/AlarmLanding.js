import React from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import BaseMain from "components/base/BaseMain";

import HeaderTitle from "components/common/HeaderTitle";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";
import AlarmCardPanel from "components/alarm/AlarmCardPanel";
import AlarmSection from "components/alarm/AlarmSection";

import "./AlarmLanding.scss";
import AlarmCardPlainPanel from "../AlarmCardPlainPanel";

// const testData = [
//   {
//     date: "2020.10.01",
//     alarmData: [
//       {
//         alarmId: "1",
//         dateTime: "2020.05.01 10:15:11",
//         woType: "BM",
//         eqpId: "W1PPKG101",
//         woNo: "",
//         alarmDesc: "M7600 Busbar Conveyor1 Stopper1 UP Time Over",
//       },
//     ],
//   },
//   {
//     date: "2020.09.28",
//     alarmData: [
//       {
//         alarmId: "2",
//         dateTime: "2020.05.01 10:15:11",
//         woType: "BM",
//         eqpId: "W1PPKG102",
//         woNo: "123456",
//         alarmDesc: "M7600 Busbar Conveyor1 Stopper1 UP Time Over",
//       },
//       {
//         alarmId: "3",
//         dateTime: "2020.05.01 10:15:11",
//         woType: "BM",
//         eqpId: "W1PPKG103",
//         woNo: "789123",
//         alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
//       },
//     ],
//   },
//   {
//     date: "2020.09.27",
//     alarmData: [
//       {
//         alarmId: "4",
//         dateTime: "2020.05.01 10:15:11",
//         woType: "BM",
//         eqpId: "W1PPKG104",
//         woNo: "",
//         alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
//       },
//     ],
//   },
// ];

const testData = [
  {
    alarmId: "1",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG101",
    woNo: "888888",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1 UP Time Over",
  },
  {
    alarmId: "2",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG102",
    woNo: "123456",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1 UP Time Over",
  },
  {
    alarmId: "3",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG103",
    woNo: "789123",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
  },
  {
    alarmId: "4",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG104",
    woNo: "",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
  },
  {
    alarmId: "5",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG105",
    woNo: "",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
  },
  {
    alarmId: "6",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG106",
    woNo: "",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
  },
  {
    alarmId: "7",
    dateTime: "2020.05.01 10:15:11",
    woType: "BM",
    eqpId: "W1PPKG107",
    woNo: "",
    alarmDesc: "M7600 Busbar Conveyor1 Stopper1",
  },
];

const Splitter = ({ text = undefined }) => (
  <div className={cx("splitter", text && "with-text")}>{text}</div>
);

const AlarmLanding = () => {
  return (
    <div className="alarm-landing">
      <FixedHeader center={<HeaderTitle text="Alarm" />} />
      <BaseMain
        style={{
          height: `calc(100vh - 56px - 60px)`,
          marginTop: `56px`,
        }}
      >
        <ul className="alarm-list">
          {testData.map((asi, idx) => (
            <li className="alarm-item" key={idx}>
              {/* <AlarmSection {...asi} /> */}
              {/* {idx !== testData.length - 1 && <Splitter />} */}
              <AlarmCardPlainPanel
                {...asi}
                isFirstNewWO={idx === testData.findIndex((data) => !data.woNo)}
              />
            </li>
          ))}
        </ul>
        {/* <ul className="alarm-list-items">
          <li className="alarm-list-item">
            <AlarmCardPanel />
          </li>
          <Splitter />
          <li className="alarm-list-item">
            <AlarmCardPanel />
          </li>
          <Splitter text="최근 30일 동안의 알람만 확일하실 수 있습니다." />
        </ul> */}
      </BaseMain>
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default AlarmLanding;
