import React from "react";

import AlarmCardPlainPanel from "components/alarm/AlarmCardPlainPanel";

import "./AlarmSection.scss";

const AlarmSection = ({ date, alarmData }) => {
  return (
    <div className="alarm-section">
      <div className="date-title">{date}</div>
      {alarmData.map((alarm) => (
        <AlarmCardPlainPanel key={alarm.alarmId} {...alarm} />
      ))}
    </div>
  );
};

export default AlarmSection;
