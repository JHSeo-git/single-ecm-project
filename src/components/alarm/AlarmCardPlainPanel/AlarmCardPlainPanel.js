import React, { useState } from "react";

import DimedModal from "components/base/DimedModal";
import OKCancelModal from "components/modal/OKCancelModal";
import AlarmWOButton from "components/alarm/AlarmWOButton";

import "./AlarmCardPlainPanel.scss";

const AlarmCardPlainPanel = ({
  alarmId,
  dateTime,
  woType,
  eqpId,
  woNo,
  alarmDesc,
  isFirstNewWO,
}) => {
  const [mode, setMode] = useState("");
  const onClick = () => {
    if (woNo) return;

    setMode("NEW_WO");
  };
  return (
    <>
      <div className="alarm-card-plain-panel">
        <div className="header-row">
          <div className="date-time">{dateTime}</div>
          <AlarmWOButton
            woNo={woNo}
            onClick={onClick}
            viewTooltip={isFirstNewWO}
          />
        </div>
        <div className="wo-title">{`[${woType}]${eqpId}`}</div>
        <div className="alarm-desc">{alarmDesc}</div>
      </div>
      {mode === "NEW_WO" && (
        <DimedModal className="modal-top-max">
          <OKCancelModal
            title="WO를 생성합니다"
            onConfirm={() => console.log("new wo")}
            onClose={() => setMode("")}
          >
            <span className="new-wo-content">{`확인을 누르면 '[${woType}]${eqpId}'의 WO가 생성됩니다.`}</span>
          </OKCancelModal>
        </DimedModal>
      )}
    </>
  );
};

export default AlarmCardPlainPanel;
