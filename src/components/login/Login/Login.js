import React, { useState } from "react";
import cx from "classnames";

import InputSelector from "components/common/InputSelector";
import DimedModal from "components/base/DimedModal";
import OKCancelModal from "components/modal/OKCancelModal";

import logo from "asset/images/img-logo.png";
import {
  IconLogoText,
  IconDelete12,
  IconEyehide,
  IconCommBack,
  IconExclamation,
  IconBoldCheck,
  IconEye,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./Login.scss";

const Login = () => {
  // email regex
  const emailRegex = new RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/
  );

  const [mode, setMode] = useState("EMAIL");

  const [email, setEmail] = useState("");
  const [isEmailValid, setIsEmailValid] = useState(false);
  const onChangeEmail = (e) => {
    const {
      target: { value },
    } = e;
    setEmail(value);
    setIsEmailValid(emailRegex.test(value));
  };
  const [pw, setPw] = useState("");
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const onChangePw = (e) => {
    const {
      target: { value },
    } = e;
    setPw(value);
    //test
    setIsPasswordValid(value.length % 2 === 0);
  };

  const [pwVisible, setPwVisible] = useState(false);
  const onTogglePwVisible = () => {
    setPwVisible(!pwVisible);
  };

  const onLoginNext = () => {
    if (email && isEmailValid) {
      setMode("PW");
    }
  };

  const [loginError, setLoginError] = useState({
    isError: false,
    name: "SSO인증실패",
    message: "SSO인증에 실패하여 앱을 종료합니다.",
  });
  const onLogin = (e) => {
    e.preventDefault();
    setLoginError({
      ...loginError,
      isError: true,
    });
  };
  return (
    <div className="login-wrapper">
      <div className="logo">
        <img src={logo} alt="logo" />
        <IconLogoText width="60" height="12" />
      </div>
      <div className="introduction-text">
        <span className="lg">LG</span>
        <span>앱소개글영역입니다</span>
      </div>
      <div className="logo-text">
        <IconLogoText width="104" height="21" fill="#000000" />
        <span>EMS</span>
      </div>
      <div className="login-text">
        <div className="login">
          <span>Login</span>
        </div>
        <div className="join">
          <span className="join-notice">New Here?</span>
          <span className="join-link">Create an account</span>
        </div>
      </div>
      <form className="login-form">
        {mode === "EMAIL" && (
          <>
            <div className="input-box login-id">
              <div className="input-label">
                <span>ID</span>
              </div>
              <div className="input-content">
                <input
                  type="email"
                  className={cx("login", email && !isEmailValid && "invalid")}
                  placeholder="Email"
                  value={email}
                  onChange={onChangeEmail}
                  autoFocus
                />
                {isEmailValid
                  ? email && (
                      <IconBase className="ab" onClick={() => setEmail("")}>
                        <IconBase className="icon-circle">
                          <IconDelete12 />
                        </IconBase>
                        <IconBase className="icon-circle good last">
                          <IconBoldCheck
                            width="9.5"
                            height="7.5"
                            fill="#ffffff"
                            opacity="1"
                          />
                        </IconBase>
                      </IconBase>
                    )
                  : email && (
                      <IconBase className="ab icon-circle alert">
                        <IconExclamation />
                      </IconBase>
                    )}
              </div>
              {email && !isEmailValid && (
                <div className="input-error">
                  <span>이메일형식이 아닙니다.</span>
                </div>
              )}
            </div>
            <button
              className="button-container next-button"
              onClick={onLoginNext}
            >
              <span>Next</span>
            </button>
          </>
        )}

        {mode === "PW" && (
          <>
            <div className="input-box login-pw">
              <div className="input-label flex-box">
                <IconBase onClick={() => setMode("EMAIL")}>
                  <IconCommBack width="10" height="16" fill="#424952" />
                </IconBase>
                <span>{email}</span>
              </div>
              <div className="input-content">
                <input
                  type={pwVisible ? "text" : "password"}
                  className={cx("login", pw && !isPasswordValid && "invalid")}
                  placeholder="Password"
                  value={pw}
                  onChange={onChangePw}
                />
                <IconBase className="ab">
                  <IconBase onClick={onTogglePwVisible}>
                    {pwVisible ? <IconEye /> : <IconEyehide />}
                  </IconBase>
                  {pw && !isPasswordValid && (
                    <IconBase className="icon-circle alert last">
                      <IconExclamation />
                    </IconBase>
                  )}
                </IconBase>
              </div>
              {pw && !isPasswordValid && (
                <div className="input-error">
                  <span>Error Text(Optional)</span>
                </div>
              )}
            </div>
            <button
              type="submit"
              className="button-container next-button full-width"
              onClick={onLogin}
            >
              <span>Login</span>
            </button>
          </>
        )}
      </form>
      {loginError && loginError.isError && (
        <DimedModal className="modal-top-max modal-error-wrapper">
          <OKCancelModal
            title={loginError.name}
            onConfirm={() => setLoginError({ ...loginError, isError: false })}
          >
            <div className="error-message">
              <span>{loginError.message}</span>
            </div>
          </OKCancelModal>
        </DimedModal>
      )}
    </div>
  );
};

export default Login;
