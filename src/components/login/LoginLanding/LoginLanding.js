import React from "react";

import logo from "asset/images/img-logo-vector.svg";
import IconBase from "components/common/IconBase";
import { IconLogoText, IconLogoTextEMS } from "components/common/Icons";

import "./LoginLanding.scss";

const LoginLanding = () => {
  return (
    <div className="login-landing">
      <div className="login-box">
        <div className="logo-wrapper">
          <img src={logo} alt="singlex logo" />
          <IconBase className="icon-singlex">
            <IconLogoText />
          </IconBase>
          <IconBase className="icon-ems">
            <IconLogoTextEMS />
          </IconBase>
        </div>
        <button className="login-button">Login</button>
      </div>
    </div>
  );
};

export default LoginLanding;
