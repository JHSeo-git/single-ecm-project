import React from "react";
import ButtonList from "components/guide/ButtonList";
import IconsList from "components/guide/IconsList";
import {createGlobalStyle} from "styled-components";

const GlobalStyle = createGlobalStyle`
  body,html{
    overflow:scroll;
    height:100%;
  }
`

const ComponentGuide = () => {
  return (
    <>
      <GlobalStyle />
      <ButtonList />
      <IconsList />
      </>
  );
};

export default ComponentGuide;
