import React from "react";

import "./WorkOrderBMDiagnosisCauseTypeB.scss";

const WorkOrderBMDiagnosisCauseTypeB = ({
  id,
  cause,
  resolution,
  successRate,
}) => {
  return (
    <div className="work-order-bm-diagnosis-cause-typeB">
      <div className="infos flex-col">
        <div className="info-line">
          <span className="label">Cause</span>
          <span className="content">{cause}</span>
        </div>
        <div className="info-line">
          <span className="label">Resolution</span>
          <span className="content">{resolution}</span>
        </div>
        <div className="info-line">
          <span className="label">Success Rate</span>
          <span className="content">{successRate}%</span>
        </div>
      </div>
    </div>
  );
};

export default WorkOrderBMDiagnosisCauseTypeB;
