import React from "react";

import FixedHeader from "components/base/FixedHeader";
import HeaderTitle from "components/common/HeaderTitle";

import IconBase from "components/common/IconBase";
import { IconSelectArrow, IconClosePopup } from "components/common/Icons";
import Button from "components/common/Button";
import CalendarMonth from "components/workOrder/CalendarMonth";

import "./WorkOrderCalendar.scss";

const WorkOrderCalendar = ({ style, onClick, calenderValue }) => {
  return (
    <>
      <div className="calendar_fix_modal" style={style}>
        <div className="calendar_fm_content">
          <FixedHeader
            left={
              <IconBase onClick={onClick}>
                <IconClosePopup strokeFill="#2C3238" />
              </IconBase>
            }
            center={
              <HeaderTitle
                text={
                  calenderValue.selectedValue
                    ? calenderValue.selectedValue
                    : calenderValue.dateType
                }
              />
            }
            right={<button>오늘</button>}
          />

          <div className="contents">
            <div className="calendar-title">
              <h1>3월</h1>
              <div className="btn-next-prev-month">
                <button className="btn-prev-month">
                  <IconSelectArrow strokeFill="#2C3238" />
                </button>
                <button className="btn-next-month">
                  <IconSelectArrow strokeFill="#2C3238" />
                </button>
              </div>
            </div>

            <CalendarMonth />

            <hr className={"border20"} />

            <div className="select-time-title">Time</div>

            <div className="select-time-wrap">
              <div className={"btn-time"}>
                <button className={"active"}>AM</button>
                <button>PM</button>
              </div>

              <div className={"select-time"}>
                <ul>
                  <li className={"active"}>09 : 00</li>
                  <li>09 : 05</li>
                  <li>09 : 10</li>
                  <li>09 : 15</li>
                  <li>09 : 20</li>
                  <li>09 : 25</li>
                  <li>09 : 30</li>
                  <li>09 : 35</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="comm-fix-bottom">
            <Button fullWidth onClick={onClick}>
              확인
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default WorkOrderCalendar;
