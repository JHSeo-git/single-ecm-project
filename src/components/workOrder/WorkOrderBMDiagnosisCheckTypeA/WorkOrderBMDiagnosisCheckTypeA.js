import React, { useState } from "react";

import "./WorkOrderBMDiagnosisCheckTypeA.scss";

const WorkOrderBMDiagnosisCheckTypeA = ({
  id,
  content,
  measure,
  measureType,
  upperBound,
  controlUppderBound,
  controlLowerBound,
  lowerBound,
  uom,
  valueList,
  value,
  comment,
  result,
}) => {
  return (
    <div className="work-order-bm-diagnosis-check-typeA">
      <div className="infos flex-col">
        <div className="info-line self-end labeled">
          <span className="uom">UOM</span>
          <span className="uom-value">{uom}</span>
        </div>
        <div className="info-line">
          <div className="info-box">
            <div className="info-title">
              Upper
              <br />
              Bound
            </div>
            <div className="info-content">
              <span className="value">{upperBound}</span>
              <span className="unit"></span>
            </div>
          </div>
          <div className="info-box">
            <div className="info-title">
              Control
              <br />
              Upper Bound
            </div>
            <div className="info-content">
              <span className="value">{controlUppderBound}</span>
              <span className="unit"></span>
            </div>
          </div>
          <div className="info-box">
            <div className="info-title">
              Control
              <br />
              Lower Bound
            </div>
            <div className="info-content">
              <span className="value">{controlLowerBound}</span>
              <span className="unit"></span>
            </div>
          </div>
          <div className="info-box">
            <div className="info-title">
              Lower
              <br />
              Bound
            </div>
            <div className="info-content">
              <span className="value">{lowerBound}</span>
              <span className="unit"></span>
            </div>
          </div>
        </div>
      </div>
      <div className="infos flex-col">
        <div className="info-line labeled">
          <span className="label">Measure</span>
          <span className="content">{measure}</span>
        </div>
        <div className="info-line labeled">
          <span className="label">Measure Type</span>
          <span className="content">{measureType}</span>
        </div>
        <div className="info-line labeled">
          <span className="label">Value List</span>
          <span className="content">
            {!valueList || valueList.length === 0 ? "-" : "map()"}
          </span>
        </div>
      </div>
    </div>
  );
};

export default WorkOrderBMDiagnosisCheckTypeA;
