import React, {useState} from "react";
import classnames from 'classnames'
import {IconLocation} from "../../../common/Icons";

const BMSpIssueItem = ({onClick}) => {
  const [todoItems, setTodoItems] = useState([
    {
      id: 1,
      no:10,
      location:"BF 1",
      title: "S/P",
      text: "BMM12345",
      subText:"MOTOR,ORIENTAL MOTOR,ORIENTA",
      value1: 0,
      value2: 0,
      inputValue1:3,
      inputValue2:3,
      isChecked: false,
    },
    {
      id: 2,
      no:10,
      location:"BF 1",
      title: "S/P",
      text: "BMM12345",
      subText:"MOTOR,ORIENTAL",
      value1: 0,
      value2: 992,
      inputValue1:3,
      inputValue2:3,
      isChecked: false,
    },
    {
      id: 3,
      no:10,
      location:"BF 1",
      title: "S/P",
      text: "BMM12345",
      subText:"MOTOR,ORIENTAL",
      value1: 0,
      value2: 992,
      inputValue1:3,
      inputValue2:3,
      isChecked: false,
    },
    {
      id: 4,
      no:10,
      location:"BF 1",
      title: "S/P",
      text: "BMM12345",
      subText:"MOTOR,ORIENTAL",
      value1: 0,
      value2: 992,
      inputValue1:3,
      inputValue2:3,
      isChecked: false,
    },
  ]);

  return <div className="list-group"  >
    {
      todoItems.map((item) => (
        <div key={item.id}
         className={classnames("sp-requirements-card",(item.isChecked && "active"))}
        >
          <div className="list-group-item" onClick={onClick}>
            <div className="list-location">
              <div>
                <span className="number">No.{item.no}</span>
                <span className="location"><IconLocation fill={"#E0205C"} width={"16px"} height={"16px"} />{item.location}</span>
              </div>
              <button>NEW</button>
            </div>
            <div className="list-group-header">
              <span className="title">{item.title}</span>
              <span className="text"><strong>{item.text}</strong>{item.subText}</span>
            </div>
            <div className="list-number-wrap div2">
              <span className="number-title">Item Qty</span>
              <span className="number-text">{item.value1}</span>
              <span className="bar">|</span>
              <span className="number-title">Reserved Qty</span>
              <span className="number-text">{item.value2}</span>
            </div>
            <div className="list-return">
              <ul>
                <li>
                  <span>Return Qty to Storeroom</span>
                  <input type="text" value={item.inputValue2} />
                </li>
                <li>
                  <span>Return Qty to Supplier</span>
                  <input type="text" value={item.inputValue2}/>
                </li>
              </ul>
            </div>
          </div>
        </div>
      ))
    }
  </div>;
};

export default BMSpIssueItem;
