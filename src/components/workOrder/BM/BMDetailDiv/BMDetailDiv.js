import React from 'react';
import FixedFooter from "../../../base/FixedFooter";
import Button from "../../../common/Button";
import {IconCalendarFull} from "../../../common/Icons";

const BmDetailDiv = () => {
  return (
    <div className="bm-detail-box">

      <div className="bmdb-title">
        <h2>
          Employee<span className="total-user">5</span>
        </h2>
      </div>

      <div className="bmdb-worker-round-list" style={{display:"none"}}>
        <div className="active">
          <span>홍</span>
          <span>홍길동</span>
        </div>
        <div>
          <span>남궁</span>
          <span>김사원</span>
        </div>
        <div>
          <span>A</span>
          <span>Courtney <br />Henry</span>
        </div>
        <div>
          <span>A</span>
          <span>ABCDEFGHIJK LMNOPQRSTU</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
        <div>
          <span>오</span>
          <span>오책임</span>
        </div>
      </div>

      <hr className="border8" style={{display:"none"}} />

      <div className="worker-time-list">
        <div className="bm-date-title">
          <div className="worker-round-list">
            <div className={"color01"}>{/* color01 ~ color06 */}
              <span>ME</span>
              <span>홍길동</span>
            </div>
            <div className="time-label">60 Min</div>
          </div>
        </div>

        <dl className="time-list">
          <dt>시간</dt>
          <dd>
            <span>09:00 AM</span>
            <span>-</span>
            <span>09:00 AM</span>

            <span className="txt-minute">20 Min</span>
          </dd>
        </dl>

        <div className="bm-date-title">
          <div className="worker-round-list">
            <div className={"color02"}>
              <span>김</span>
              <span>김사원</span>
            </div>
            <div className="time-label">60 Min</div>
          </div>
        </div>

        <dl className="time-list">
          <dt>시간</dt>
          <dd>
            <span>09:00 AM</span>
            <span>-</span>
            <span>09:00 AM</span>

            <span className="txt-minute">20 Min</span>
          </dd>
          <dd>
            <span>09:00 AM</span>
            <span>-</span>
            <span>09:00 AM</span>

            <span className="txt-minute">20 Min</span>
          </dd>
        </dl>

        <div className="bm-date-title">
          <div className="worker-round-list">
            <div className={"color03"}>
              <span>한</span>
              <span>한선임</span>
            </div>
            <div className="time-label">60 Min</div>
          </div>
        </div>

        <dl className="time-list">
          <dt>시간</dt>
          <dd>
            <span>09:00 AM</span>
            <span>-</span>
            <span>09:00 AM</span>

            <span className="txt-minute">20 Min</span>
          </dd>
        </dl>

        <div className="bm-date-title">
          <div className="worker-round-list">
            <div className={"color03"}>
              <span>남궁</span>
              <span>남궁사원</span>
            </div>
            <div className="time-label">60 Min</div>
          </div>
        </div>

        <dl className="time-list">
          <dt>시간</dt>
          <dd>
            <span>09:00 AM</span>
            <span>-</span>
            <span>09:00 AM</span>

            <span className="txt-minute">20 Min</span>
          </dd>
        </dl>
      </div>
    </div>
  );
};

export default BmDetailDiv;