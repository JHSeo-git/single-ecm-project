import React, { useState,useEffect } from "react";

import FixedHeader from "components/base/FixedHeader";
import BaseMain from "components/base/BaseMain";
import NoResult from "components/common/NoResult";
import { IconCommBack, IconSearchBig } from "components/common/Icons";
import SearchDetailHeaderTypeA from "components/search/SearchDetailHeaderTypeA";
import "./BMSPSearch.scss";
import IconBase from "../../../common/IconBase";

import { testData } from "lib/api/test";
import SearchResultCardInventoryTypeC from "../../../search/SearchResultCardInventoryTypeC";

const BMSPSearch = ({ type, onClose, initValue = "",onSearchClick, onDetailClick, onClick }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "storeroom", text: "central" },
    { id: "BMBOLT0001", type: "itemCode", text: "BMBOLT0001" },
    { id: "BMBOLT0002", type: "itemCode", text: "BMBOLT0002" },
    { id: "BMBOLT0003", type: "itemCode", text: "BMBOLT0003" },
  ]);
  const [results, setResults] = useState([
    {
      itemCode: "BMBOLT000111111111111",
      storeroom: "centralcentralcentral",
      description: "Actuator, Fettle, Premium linear",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0002",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000HBolt, Mobile, A109K TTTESTTE",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0003",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0004",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0005",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0006",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantityAvailable: "992",
    },
    {
      itemCode: "BMBOLT0007",
      storeroom: "central",
      description: "Bolt, Mobile, A109K-4000H",
      quantityAvailable: "992",
    },
  ]);

  const [selectedIdx, setSelectedIdx] = useState(-1);

  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  const [testApiData, setTestApiData] = useState(testData);

  const [searchKeywords, setSearchKeywords] = useState("");
  const [searchInputValue, setSearchInputValue] = useState(initValue);
  const [searchType, setSearchType] = useState();
  const [searchTerm, setSearchTerm] = useState("");

  const onSearchFormBack = () => {
    // hide page
    onClose();
  };
  const onSearchConfirm = () => {
    // TODO: add search conditions(searchKeywords => keywords)
    // hide page
  };

  const onEnterKeyPress = (e) => {
    if (e.key === "Enter") {
      setSearchTerm(searchInputValue);
    }
  };

  const onSearch = () => {
    setSearchTerm(searchInputValue);
  };

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setSearchInputValue(value);
  };

  const onAddClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;

    setSearchKeywords([
      ...searchKeywords,
      ...testApiData.filter((item) => item.id === id),
    ]);
  };

  const onInputAllClearClick = () => {
    setSearchKeywords([]);
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const onBlur = () => {
    setIsKeyboardFocus(false);
  };
  const onFocus = () => {
    setIsKeyboardFocus(true);
  };

  const initialize = () => {
    setSearchType(type);

    setSearchKeywords(keywords.filter((keyword) => keyword.type === type));

    if (initValue) {
      onSearch();
    }
  };

  useEffect(() => {
    initialize();
  }, []);


  return (
    <div className="search-form typeC">
      <FixedHeader
        left={
          <IconBase onClick={onSearchFormBack}>
            <IconCommBack />
          </IconBase>
        }
        center={
          keywords && (
            <SearchDetailHeaderTypeA
              searchInputValue={searchInputValue}
              onChange={onChange}
              onEnterKeyPress={onEnterKeyPress}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          )
        }
        right={
          <IconBase onClick={onSearch}>
            <IconSearchBig />
          </IconBase>
        }
        centerScrollX={true}
      />
      <BaseMain
        isMenuOpen={isMenuOpen}
        isModalOpen={selectedIdx > -1}
        onMenuClick={onMenuClick}
        style={{
          height: `calc(100vh - 56px)`,
          marginTop: `calc(56px)`,
        }}
      >
        <div className="result-panel">
          {results?.length === 0 ? (
            <NoResult title1="No Result!" subTitle1="항목이 없습니다!" />
          ) : (
            <ul className="result-items">
              {results.map((result, idx) => (
                <li key={idx}>
                  <SearchResultCardInventoryTypeC
                    itemCode={result.itemCode}
                    storeroom={result.storeroom}
                    description={result.description}
                    quantity={result.quantity}
                    quantityAvailable={result.quantityAvailable}
                  />
                </li>
              ))}
            </ul>
          )}
        </div>
      </BaseMain>
    </div>
  );
};

export default BMSPSearch;
