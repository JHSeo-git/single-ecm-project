import React,{useState} from 'react';
import { format } from "date-fns";
import moment from "moment";
import cx from "classnames";

import "./BMIssueDetailDiv.scss"
import LabelInputTypeA from "../../../search/LabelInputTypeA";
import LabelInputTypeC from "../../../search/LabelInputTypeC";
import LabelInputTypeD from "../../../search/LabelInputTypeD";
import {
  IconArrowRightBig, IconClock,
  IconComment, IconRoundClose,
  IconRoundPlus,
  IconTrashCan
} from "components/common/Icons";
import FixedFooter from "../../../base/FixedFooter";
import Button from "../../../common/Button";
import BMSelectCommonModal from "../BMSelectCommonModal";
import ScrollableModal from "../../../base/ScrollableModal";
import ModalFullPage from "../../../common/ModalFullPage";
import BMSPSearch from "../BMSPSearch";
import SwitchButton from "../../../common/SwitchButton";
import LabelSelectBox from "../../LabelSelectBox";
import IconBase from "../../../common/IconBase";
import Counter from "../../../common/Counter";


const BMIssueDetailDiv = () => {
  const [keywords, setKeywords] = useState([
    { id: "1", type: "Equipment Id", text: "Equipment 1"},
  ]);
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const [isSPDetail, setIsSPDetail] = useState(false);
  const onInputClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { type },
    } = currentTarget;
  };

  const [isSPSearch, setIsSPSearch] = useState(false);

  const [isRoundTagToggle, setRoundTagToggle] = useState(false);
  const onRoundTagToggle = (e) => {
    setRoundTagToggle(!isRoundTagToggle);
  };

  const [isShowSelectBox, setIsShowSelectBox] = useState(false);
  const onSelectModal = () => {
    setIsShowSelectBox(!isShowSelectBox);
  };

  const [isEditQuantity, setIsEditQuantity] = useState(false);
  const onIsEditQuantity = () => {
    setIsEditQuantity(!isEditQuantity);
  };

  const [selectedIdx, setSelectedIdx] = useState(false);
  const onCancelSelect = (e) => {
    console.log(e);
    setSelectedIdx(false);
  };

  const [isAddIssue, setIsAddIssue] = useState(false);
  const onAddIssue = (e) => {
    setIsAddIssue(!isAddIssue);
  }

  const [isCreatedBox, setIsCreatedBox] = useState(false);
  const onCreatedBox = (e) => {
    setIsCreatedBox(!isCreatedBox);
  }

  const [isDeleteIssue, setIsDeleteIssue] = useState(false);
  const onDeleteActive = () =>{
    setIsDeleteIssue(!isDeleteIssue)
    setIsCreatedBox(!isCreatedBox);
  }
  return (
    <div className="bmsp-wrap">
      <div className="issue-wrap">
        <div>
          <span>Issue</span>
          <div className="count-circle">
            <div className="circle-text">7</div>
          </div>
        </div>
        <div className="btn-delete-wrap" onClick={onDeleteActive} >
          <IconTrashCan fill="#6B7682" />
          <SwitchButton />
        </div>
      </div>

      <div className="issue-add">
        <button className={cx("btn-add", isAddIssue && "active")} onClick={onAddIssue}>
          {isAddIssue ? <IconRoundPlus fill="#E0205C" /> : <IconRoundPlus />}
          Add
        </button>
        <div className={cx("created-box", isCreatedBox && "active")} onClick={onCreatedBox}>
          <div className="up-line">
            <div className={cx("card-type","unselected")}>미선택</div>
          </div>
          <div className="description">Equipment 1</div>
          {isDeleteIssue && <button className={cx("btn-delete")}><IconRoundClose /></button>}
        </div>
        <div className={cx("created-box", isCreatedBox && "active")} onClick={onCreatedBox}>
          <div className="up-line">
            <div className={cx("card-type","input-card")}>123465789</div>
          </div>
          <div className="description">Equipment 1</div>
          {isDeleteIssue && <button className={cx("btn-delete")}><IconRoundClose /></button>}
        </div>
        <div className={cx("created-box","max", isCreatedBox && "active")} onClick={onCreatedBox}>
          <div className="up-line">
            <div className={cx("card-type","input-card")}>123465789</div>
            <div className={cx("card-type","input-card")}>123465789</div>
          </div>
          <div className="description">Equipment 1</div>
          {isDeleteIssue && <button className={cx("btn-delete")}><IconRoundClose /></button>}
        </div>
        <div className={cx("created-box","wide", isCreatedBox && "active")} onClick={onCreatedBox}>
          <div className="up-line">
            <div className={cx("card-type","input-card")}>123465789</div>
            <div className={cx("card-type","input-card")}>123465789</div>
            <div className={cx("card-type","input-card")}>123465789</div>
          </div>
          <div className="description">Equipment 1</div>
          {isDeleteIssue && <button className={cx("btn-delete")}><IconRoundClose /></button>}
        </div>
      </div>

      <div className="bmsp-div2">
        <dl>
          <dt>Item Qty</dt>
          <dd>2</dd>
        </dl>
        <dl>
          <dt>Issue  Qty</dt>
          <dd>2</dd>
        </dl>
      </div>

      <div className="bmsp-contents">
        <div className="label-input">
          <LabelInputTypeD
            labelText="Equipment Id"
            keywords={keywords}
            setKeywords={setKeywords}
            keywordType="equipmentId"
            onInputClick={onInputClick}
            setKeyboardFocus={setIsKeyboardFocus}
            necessary={<span className={"necessary-dot"}></span>}
          />

          <div className="label-input-else">
            <LabelInputTypeD
              labelText="Desorption S/N"
              keywords={keywords}
              setKeywords={setKeywords}
              keywordType="equipmentId"
              onInputClick={onInputClick}
              setKeyboardFocus={setIsKeyboardFocus}
              necessary={<span className={"necessary-dot"}></span>}
            />
          </div>

          <div className="label-input">
            <label>Quantity<span className={"necessary-dot"}></span></label>
            <div className="edit-quantity-wrap" onClick={onIsEditQuantity}>
              <button>Edit Quantity<IconArrowRightBig /></button>
            </div>
          </div>
        </div>

        <div className="label-input">
          <LabelSelectBox labelText="Replacement Category"
            onClick={onSelectModal}
            defaultText="주기도달"
          />
        </div>

        <div className="label-input">
          <LabelSelectBox labelText="Replacement Reason"
            onClick={onSelectModal}
            defaultText="Arcing"
          />
        </div>

        <hr className="border3" />

        <LabelInputTypeC
          labelText="Comment"
          keywords={keywords}
          setKeywords={setKeywords}
          keywordType="equipmentId"
          onInputClick={onInputClick}
          setKeyboardFocus={setIsKeyboardFocus}
          iconType={<IconComment />}
        />
      </div>

      <FixedFooter>
        <div className="bottom-btn-wrap">
          <Button color="gray" fullWidth>Save</Button>
        </div>
      </FixedFooter>

      {isShowSelectBox && (
        <BMSelectCommonModal onClick={onSelectModal} />
      )}

      {isEditQuantity && (
        <ScrollableModal onCick={onIsEditQuantity}>
          <div className="edit-quantity-modal">
            <div className="eq-list-item">
              <label>Repair Qty</label>
              <Counter type="typeWide" size="12px" />
            </div>
            <div className="eq-list-item">
              <label>Discard Qty</label>
              <Counter type="typeWide" size="12px" />
            </div>
            <div className="eq-list-item">
              <label>Not Deassign Qty</label>
              <Counter type="typeWide" size="12px" />
            </div>

            <div className="reason-write">
              <label>Reason for Not Deassign</label>
              <LabelInputTypeC />
            </div>

            <div className="eq-list-item">
              <label>UNL Qty</label>
              <Counter type="typeWide" size="12px" />
            </div>
            <div className="eq-list-item">
              <label>Additional Qty</label>
              <Counter type="typeWide" size="12px" />
            </div>
          </div>
        </ScrollableModal>
      )}

    </div>

  );
};

export default BMIssueDetailDiv;