import React, {useState} from 'react';
import "./BMdarkDiv.scss"
const BMdarkDiv = ({nowsTitle,nowTitle,timeTag, timetext, spNDesc}) => {

  const [spResults, setspResults] =useState([
    {
      id:1,
      sp:"BMBOLT0003",
      Desc:"Bolt, Mobile, A109K-4000H"
    }
  ])

  return (
    <div className="bm-dark-box">
      {nowsTitle && <div className="now-s-title">{nowsTitle}</div>}
      <div className="now-title">
        <span className="title">{nowTitle}</span>
        {timeTag && <span className="time-tag">{timetext} Min</span>}
      </div>
      {spNDesc &&
        <ul className="spNDesc-list">
          {
            spResults.map((item) => (
              <>
                <li>
                  <span>S/P</span>
                  <span>{item.sp}</span>
                </li>
                <li>
                  <span>Desc</span>
                  <span>Bolt, Mobile, A109K-4000H</span>
                </li>
                </>
            ))
          }
        </ul>
      }
    </div>
  );
};

export default BMdarkDiv;