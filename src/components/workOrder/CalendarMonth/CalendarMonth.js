import React from "react";

import "./CalendarMonth.scss";

const CalendarMonth = ({ year, monthText }) => {
  return (
    <>
      <div className="calendar_tb">
        <table>
          <colgroup>
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
            <col width="14%" />
          </colgroup>
          <tbody>
            <tr>
              <td>
                <span className="prev_day">1</span>
              </td>
              <td>
                <span className="prev_day">2</span>
              </td>
              <td>
                <span className="prev_day">3</span>
              </td>
              <td>
                <span className="prev_day">4</span>
              </td>
              <td>
                <span className="prev_day">5</span>
              </td>
              <td>
                <span className="prev_day">6</span>
              </td>
              <td>
                <span className="today">7</span>
              </td>
            </tr>
            <tr>
              <td className="term-start">
                <span className="select_day from">8</span>
              </td>
              <td className="term">
                <span>9</span>
              </td>
              <td className="term">
                <span>10</span>
              </td>
              <td className="term">
                <span>11</span>
              </td>
              <td className="term">
                <span>12</span>
              </td>
              <td className="term-end">
                <span className="select_day">13</span>
              </td>
              <td>
                <span>14</span>
              </td>
            </tr>
            <tr>
              <td>
                <span>15</span>
              </td>
              <td>
                <span>16</span>
              </td>
              <td>
                <span>17</span>
              </td>
              <td>
                <span>18</span>
              </td>
              <td>
                <span>19</span>
              </td>
              <td>
                <span>20</span>
              </td>
              <td>
                <span>21</span>
              </td>
            </tr>
            <tr>
              <td>
                <span>22</span>
              </td>
              <td>
                <span>23</span>
              </td>
              <td>
                <span>24</span>
              </td>
              <td>
                <span>25</span>
              </td>
              <td>
                <span>26</span>
              </td>
              <td>
                <span>27</span>
              </td>
              <td>
                <span>28</span>
              </td>
            </tr>
            <tr>
              <td>
                <span>29</span>
              </td>
              <td>
                <span>30</span>
              </td>
              <td>
                <span>31</span>
              </td>
              <td>
                <span>&nbsp;</span>
              </td>
              <td>
                <span>&nbsp;</span>
              </td>
              <td>
                <span>&nbsp;</span>
              </td>
              <td>
                <span>&nbsp;</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};

export default CalendarMonth;
