import React, { useState } from "react";

import FixedFooter from "components/base/FixedFooter";
import WorkOrderToDoItemTypeA from "components/workOrder/WorkOrderToDoItemTypeA";
import WorkOrderStepButtonTypeA from "components/workOrder/WorkOrderStepButtonTypeA";

import "./WorkOrderManageSafetyPlan.scss";

const WorkOrderManageSafetyPlan = ({ onNextClick }) => {
  const [todoItems, setTodoItems] = useState([
    {
      id: "1",
      content: "작업장 바닥은 견고하며, 미끄럽지 않은지 확인",
      isChecked: false,
    },
    {
      id: "2",
      content: "바닥면과 높이차이 확인",
      isChecked: false,
    },
    {
      id: "3",
      content: "받침대, 선반 등 손상여부 확인",
      isChecked: false,
    },
    {
      id: "4",
      content: "덮개 설치",
      isChecked: false,
    },
    {
      id: "5",
      content: "재료, 적재 상태 확인",
      isChecked: false,
    },
  ]);

  const onToDoCheckClick = (id) => {
    const newItems = todoItems.map((item) =>
      item.id === id ? { ...item, isChecked: !item.isChecked } : item
    );

    setTodoItems(newItems);
  };

  const onAllCheck = () => {
    const checkFlag =
      todoItems.filter((item) => item.isChecked === false).length > 0;

    const newItems = todoItems.map((item) => ({
      ...item,
      isChecked: checkFlag,
    }));
    setTodoItems(newItems);
  };
  return (
    <div className="work-order-manage-safetyplan">
      <div className="todo-title">
        <span className="title">Precaution</span>
        <button className="btn-todo-all" onClick={onAllCheck}>
          {todoItems.filter((item) => item.isChecked === false).length > 0
            ? "전체선택"
            : "전체해제"}
        </button>
      </div>
      <ul className="todo-items">
        {todoItems.map((item, idx) => (
          <li className="todo-item" key={idx}>
            <WorkOrderToDoItemTypeA
              itemName={item.content}
              isCheck={item.isChecked}
              onToDoCheckClick={() => onToDoCheckClick(item.id)}
            />
          </li>
        ))}
      </ul>
      <FixedFooter>
        <WorkOrderStepButtonTypeA
          content="Next Step"
          onClick={onNextClick}
          className="active"
        />
      </FixedFooter>
    </div>
  );
};

export default WorkOrderManageSafetyPlan;
