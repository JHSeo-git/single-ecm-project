import React, { useState } from "react";
import cx from "classnames";

import { IconPlus, IconMinus, IconComment } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderToDoItemTypeC.scss";

const WorkOrderToDoItemTypeC = ({
  itemId,
  itemIdx,
  itemName,
  itemResult,
  itemComment,
  className,
  onSelect,
  onComment,
  children,
}) => {
  const [isExpand, setIsExpand] = useState(false);

  const onClick = () => {
    setIsExpand(!isExpand);
  };

  const onSelectClick = (itemId, result) => {
    onSelect(itemId, result);
    setIsExpand(false);
  };

  const [inputValue, setInputValue] = useState("");

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setInputValue(value);
  };

  return (
    <div
      className={cx(
        "work-order-todo-item-typeC",
        isExpand && "expand",
        className
      )}
    >
      <div className="todo-item-typeC-header" onClick={onClick}>
        <div className="col">
          <span className="item-index">{itemIdx}</span>
          <span className={cx("item-name", !isExpand && "line-ellipse")}>
            {itemName}
          </span>
        </div>
        {itemResult ? (
          <div className="col">
            {isExpand ? (
              <IconMinus size="20" fill="#A5B3C2" />
            ) : (
              <div
                className={cx(
                  "selected-round-tag",
                  itemResult === "NG" && "alert"
                )}
              >
                {itemResult}
              </div>
            )}
          </div>
        ) : (
          <div className="col">
            {!isExpand && <span className="expand-text">더보기</span>}
            <IconBase>
              {isExpand ? (
                <IconMinus size="20" fill="#A5B3C2" />
              ) : (
                <IconPlus size="20" fill="#A5B3C2" />
              )}
            </IconBase>
          </div>
        )}
      </div>
      {isExpand && (
        <div className="todo-item-typeC-content">
          <div className="spliter mt-none" />
          {children}
          <div className="spliter" />
          <div className="todo-item-labeled">
            <span className="label">Value</span>
            <input
              className="input-content"
              type="number"
              placeholder="Value"
              value={inputValue}
              onChange={onChange}
            />
          </div>
          <div className="todo-item-labeled">
            <span className="label">Comments</span>
            <IconBase
              className={cx("comment", itemComment?.length > 0 && "more")}
              onClick={onComment}
            >
              <IconComment />
            </IconBase>
          </div>
          <div className="todo-item-btns">
            <button
              className={cx(
                "btn-check-select",
                "ng-btn",
                itemResult === "NG" && "selected"
              )}
              onClick={() => onSelectClick(itemId, "NG")}
            >
              <span>NG</span>
            </button>
            <button
              className={cx(
                "btn-check-select",
                "ok-btn",
                itemResult === "OK" && "selected"
              )}
              onClick={() => onSelectClick(itemId, "OK")}
            >
              <span>OK</span>
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default WorkOrderToDoItemTypeC;
