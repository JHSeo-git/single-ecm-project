import React from "react";

import { IconAttach } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import "./AttachedFileTextBox.scss";

const AttachedFileTextBox = ({ text, ...rest }) => {
  return (
    <div className="attahced-file-text-box" {...rest}>
      <IconBase>
        <IconAttach />
      </IconBase>
      <span className="line-ellapse">{text}</span>
    </div>
  );
};

export default AttachedFileTextBox;
