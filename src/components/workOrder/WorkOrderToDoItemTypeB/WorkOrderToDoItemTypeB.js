import React, { useState } from "react";
import cx from "classnames";

import {
  IconPlus,
  IconMinus,
  IconMainReleaseFull,
  IconCheck,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderToDoItemTypeB.scss";

const WorkOrderToDoItemTypeB = ({
  itemIdx,
  itemName, //content
  itemSelected, //isSelected
  className,
  onSelect,
  children,
}) => {
  const [isExpand, setIsExpand] = useState(false);

  const onClick = () => {
    setIsExpand(!isExpand);
  };

  const onSelectClick = () => {
    onSelect();
    setIsExpand(false);
  };

  return (
    <div
      className={cx(
        "work-order-todo-item-typeB",
        isExpand && "expand",
        className
      )}
    >
      <div className="todo-item-typeB-header" onClick={onClick}>
        <div className="col">
          <span className="item-index">{itemIdx}</span>
          <span className={cx("item-name", !isExpand && "line-ellipse")}>
            {itemName}
          </span>
        </div>
        {itemSelected ? (
          <div className="col">
            {isExpand ? (
              <IconMinus size="20" fill="#A5B3C2" />
            ) : (
              <>
                <IconBase>
                  <IconMainReleaseFull fill="#26B9D1" width="21" height="21" />
                </IconBase>
                <span className="selected-text">선택</span>
              </>
            )}
          </div>
        ) : (
          <div className="col">
            {!isExpand && <span className="expand-text">더보기</span>}
            <IconBase>
              {isExpand ? (
                <IconMinus size="20" fill="#A5B3C2" />
              ) : (
                <IconPlus size="20" fill="#A5B3C2" />
              )}
            </IconBase>
          </div>
        )}
      </div>
      {isExpand && (
        <div className="todo-item-typeB-content">
          {children}
          <div className="todo-item-btns">
            <button
              className={cx("btn-item-select", itemSelected && "selected")}
              onClick={onSelectClick}
            >
              <IconBase>
                {itemSelected ? (
                  <IconCheck strokeFill="#ffffff" />
                ) : (
                  <IconCheck />
                )}
              </IconBase>
              {!itemSelected && <span>선택</span>}
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default WorkOrderToDoItemTypeB;
