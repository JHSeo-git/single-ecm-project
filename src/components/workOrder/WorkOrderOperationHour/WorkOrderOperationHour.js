import React, { useEffect, useState } from "react";
import BMWrap from "components/workOrder/BM/BMWrap";
import BMdarkDiv from "components/workOrder/BM/BMdarkDiv";
import BmDetailDiv from "components/workOrder/BM/BMDetailDiv";

const WorkOrderOperationHour = ({ onClose }) => {
  const [isSelectNowDateTag, setIsSelectNowDateTag] = useState(false);
  const selectCalendar = (e) => {
    setIsSelectNowDateTag(!isSelectNowDateTag);
  };

  return (
    <>
      <BMWrap title={"Actual Time"} onClose={onClose}>
        <BMdarkDiv nowTitle={"123675415"} />
        <BmDetailDiv />
      </BMWrap>
    </>
  );
};

export default WorkOrderOperationHour;
