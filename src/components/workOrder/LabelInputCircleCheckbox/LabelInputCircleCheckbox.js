import React from "react";

import "./LabelInputCircleCheckbox.scss";

const LabelInputCircleCheckbox = ({ style, labelText, items }) => {
  return (
    <div className="label-input-circle-check" style={style}>
      <div className="label-text">{labelText}</div>
      {items && (
        <ul className="checkbox-items">
          {items.map((item, idx) => (
            <li key={idx}>
              <div className="circle-check">
                <input id={item.id} name={item.id} type="checkbox" />
                <label htmlFor={item.id}>{item.value}</label>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default LabelInputCircleCheckbox;
