import React from "react";

import "./LabelInputCircleRadio.scss";

const LabelInputCircleRadio = ({
  style,
  radioGroupName,
  labelText,
  items,
  selectedId,
  setSelectedId,
}) => {
  return (
    <div className="label-input-circle-radio" style={style}>
      <div className="label-text">{labelText}</div>
      {items && (
        <ul className="radio-items">
          {items.map((item, idx) => (
            <li key={idx}>
              <div className="circle-radio">
                <input
                  id={`${radioGroupName}-${item.id}`}
                  type="radio"
                  name={`${radioGroupName}-${item.id}`}
                  value={item.value}
                  checked={selectedId === item.id}
                  onChange={() => setSelectedId(item.id)}
                />
                <label htmlFor={`${radioGroupName}-${item.id}`}>
                  {item.value}
                </label>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default LabelInputCircleRadio;
