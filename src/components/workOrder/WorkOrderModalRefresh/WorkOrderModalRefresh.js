import React from "react";
import classnames from "classnames"
import "components/dailyCheck/dc-common/DailyCheckCommon.scss";
import "./WorkOrderModalRefresh.scss";
import classNames from "classnames";

const WorkOrderModalRefresh = ({className,offset,children}) => {
  return (
    <WorkOrderModalRefresh className={classNames("btn", offset)}>
      {children}
    </WorkOrderModalRefresh>
  );
};

export default WorkOrderModalRefresh;
