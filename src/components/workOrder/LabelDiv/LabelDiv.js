import React from "react";
import cx from "classnames";

import "./LabelDiv.scss";

const LabelDiv = ({
  label,
  value,
  isShowBorderBottom = true,
  style,
  children,
}) => {
  return (
    <div className="wo-label-div" style={style}>
      <div className={cx("wo-label")}>{label}</div>
      <div className={cx("wo-value", isShowBorderBottom && "border-bottom")}>
        {value}
      </div>
      {children}
    </div>
  );
};

export default LabelDiv;
