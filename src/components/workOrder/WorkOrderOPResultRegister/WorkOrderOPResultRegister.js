import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseMain from "components/base/BaseMain";

import WorkOrderToDoItemTypeC2 from "components/workOrder/WorkOrderToDoItemTypeC2";
import WorkOrderBMDiagnosisCheckTypeA from "components/workOrder/WorkOrderBMDiagnosisCheckTypeA";
import WorkOrderCheckResultInsert from "components/workOrder/WorkOrderCheckResultInsert";

import HeaderTitle from "components/common/HeaderTitle";
import CircleText from "components/common/CircleText";
import IconBase from "components/common/IconBase";
import { IconClosePopup } from "components/common/Icons";

import "./WorkOrderOPResultRegister.scss";

const WorkOrderOPResultRegister = ({ onClose, onSave }) => {
  const [items, setItems] = useState([
    {
      id: "20",
      content: "PC 통신 케이블 연결 상태 점검, PC 통신 케이블 연결 상태 점검",
      measure: "육안",
      measureType: "GAUGE",
      upperBound: "5.00",
      controlUppderBound: "4.00",
      controlLowerBound: "3.00",
      lowerBound: "3.00",
      uom: "Hours",
      point: 1010,
      value: "",
      result: "", //OK , NG-ADJUST, NG-WO
      activity: "",
      measuredDate: "",
      comment: "연결상태 점검 확인, 점검 확인",
      isSelected: false,
    },
    {
      id: "21",
      content: "PC 통신 케이블 연결 상태 점검",
      measure: "육안",
      measureType: "GAUGE",
      upperBound: "5.00",
      controlUppderBound: "4.00",
      controlLowerBound: "3.00",
      lowerBound: "3.00",
      uom: "Hours",
      point: 2000,
      value: "",
      result: "", //OK , NG-ADJUST, NG-WO
      activity: "",
      measuredDate: "",
      comment: "연결상태 점검 확인, 점검 확인",
      isSelected: false,
    },
  ]);

  return (
    <div className="work-order-op-result-register">
      <FixedHeader
        left={
          <IconBase onClick={onClose}>
            <IconClosePopup />
          </IconBase>
        }
        center={
          <>
            <HeaderTitle text="점검결과 입력" />
            <CircleText text="4" />
          </>
        }
      />
      <BaseMain
        style={{
          height: `calc(100vh - (56px + 87px))`,
          marginTop: `56px`,
          background: "white",
        }}
      >
        <div className="main-inner">
          <ul className="result-items">
            {items?.map((item, idx) => (
              <li className="result-item" key={item.id}>
                <WorkOrderCheckResultInsert
                  itemId={item.id}
                  itemIdx={item.id}
                  itemName={item.content}
                  itemResult={item.result}
                  itemSelected={item.isSelected}
                  itemComment={item.comment}
                  itemPoint={item.point}
                  onSelect={null}
                >
                  <WorkOrderBMDiagnosisCheckTypeA {...item} />
                </WorkOrderCheckResultInsert>
              </li>
            ))}
          </ul>
        </div>
      </BaseMain>
      <FixedFooter className="footer-white">
        <div className="inner">
          <div className="save-button" onClick={onSave}>
            Save
          </div>
        </div>
      </FixedFooter>
    </div>
  );
};

export default WorkOrderOPResultRegister;
