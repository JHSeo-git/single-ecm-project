import React from "react";
import cx from "classnames";

import { IconLight } from "components/common/Icons";
import bmBanner from "asset/images/img-bm-banner.svg";
import pmBanner from "asset/images/img-pm-banner.svg";

import "./WorkOrderTitlePanel.scss";

const WorkOrderTitlePanel = ({ progressState, woNo, woType }) => {
  return (
    <div
      className={cx("work-order-title-panel", woType === "bm" ? "bm" : "pm")}
      style={{
        backgroundImage: `url(${woType === "bm" ? bmBanner : pmBanner})`,
      }}
    >
      <span className="work-order-badge">
        {woType === "bm" && <IconLight width="18" height="18" fill="#ffffff" />}
        <span className="text">{woType === "bm" ? "BM" : "PM"}</span>
      </span>
      <dl>
        <dt>{progressState}</dt>
        <dd>{woNo}</dd>
      </dl>
    </div>
  );
};

export default WorkOrderTitlePanel;
