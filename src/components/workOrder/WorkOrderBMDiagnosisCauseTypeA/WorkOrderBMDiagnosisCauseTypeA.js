import React, { useState } from "react";

import InputSelector from "components/common/InputSelector";
import WorkOrderSPCircleText from "components/workOrder/WorkOrderSPCircleText";

import { IconArrowRight } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./WorkOrderBMDiagnosisCauseTypeA.scss";

const WorkOrderBMDiagnosisCauseTypeA = ({
  id,
  content,
  successRate,
  leadTime,
  man,
  cause,
  resolution,
  resource,
  reference,
  description,
  reason,
  sp,
  result,
}) => {
  const [inputValueItem, setInputValueItem] = useState();
  const [selectItems, setSelectItems] = useState([
    {
      id: "1",
      name: "item1",
      value: "Lack Of Activity",
    },
    {
      id: "2",
      name: "item2",
      value: "WO Publication Mistake",
    },
    {
      id: "3",
      name: "item3",
      value: "No Activity",
    },
    {
      id: "4",
      name: "item4",
      value: "New BM",
    },
  ]);

  return (
    <div className="work-order-bm-diagnosis-cause-typeA">
      <div className="infos">
        <div className="info-box">
          <div className="info-title">Success Rate</div>
          <div className="info-content">
            <span className="value">{successRate}</span>
            <span className="unit">%</span>
          </div>
        </div>
        <div className="info-box">
          <div className="info-title">Lead Time</div>
          <div className="info-content">
            <span className="value">{leadTime}</span>
            <span className="unit">Min</span>
          </div>
        </div>
        <div className="info-box">
          <div className="info-title">Man</div>
          <div className="info-content">
            <span className="value">{man}</span>
            <span className="unit"></span>
          </div>
        </div>
      </div>
      <div className="infos flex-col">
        <div className="info-line">
          <span className="label">Cause</span>
          <span className="content">{cause}</span>
        </div>
        <div className="info-line">
          <span className="label">Resolution</span>
          <span className="content">{resolution}</span>
        </div>
        <div className="info-line">
          <span className="label">Resource</span>
          <span className="content">{resource}</span>
        </div>
        <div className="info-line">
          <span className="label">Refernce</span>
          <span className="content">{reference}</span>
        </div>
        <div className="info-line">
          <span className="label">Description</span>
          <span className="content">{description}</span>
        </div>
      </div>
      <div className="info-splitter" />
      <div className="infos flex-col">
        <div className="info-line">
          <span className="label">Reason</span>
          <InputSelector
            title="Reason"
            items={selectItems}
            inputValueItem={inputValueItem}
            setInputValueItem={setInputValueItem}
          />
        </div>
        <div className="info-line">
          <span className="label">SP</span>
          <WorkOrderSPCircleText className="sp-circle-wrapper">
            {sp ? (
              <span className="sp-text">{sp}</span>
            ) : (
              <>
                <span className="sp-text--empty">추가하기</span>
                <IconBase className="icon-wrapper">
                  <IconArrowRight width="5.7" height="9.8" fill="#6B7682" />
                </IconBase>
              </>
            )}
          </WorkOrderSPCircleText>
        </div>
      </div>
    </div>
  );
};

export default WorkOrderBMDiagnosisCauseTypeA;
