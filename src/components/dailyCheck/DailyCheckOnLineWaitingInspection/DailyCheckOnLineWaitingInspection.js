import React, {useState} from "react";
import {Link} from "react-router-dom";

import "../dc-common/DailyCheckCommon.scss";
import "./DailyCheckOnLineWaitingInspection.scss";

import Button from "components/common/Button";
import {IconDownload2, IconHamburger, IconOffLine, IconONLine,} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import FixedHeader from "components/base/FixedHeader";

import DailyCheckTab from "components/dailyCheck/dc-common/DailyCheckTab";
import { DailyCheckTabButton } from "components/dailyCheck/dc-common/DailyCheckTab/DailyCheckTab";
import MessageBar from "components/common/MessageBar";
import MenuNav from "components/base/MenuNav";
import DailyCommonRoundBox from "components/dailyCheck/DailyCommonRoundBox/DailyCommonRoundBox";
import PieChartCommon from "../../common/PieChartCommon";
import SuccessLottie from "../../common/SuccessLottie";
import DailyCheckModalSuccess from "../DailyCheckModalSuccess";

const DailyCheckOnLineWaitingInspection = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <>
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
        center={
          <div className="header-icon">
            <div className="daily-check-title">
              <span>일상점검 계획</span>
            </div>
          </div>
        }
        right={
          <IconBase>
            <IconDownload2 fill="#2C3238" />
          </IconBase>
        }
      />

      <div className="contents bg-type">
        {isMenuOpen && <MenuNav onClick={onMenuClick} />}
        <DailyCheckTab>
          <Link to={"./daily-check-waiting-inspection"}>
            <DailyCheckTabButton
              tabTitle="OFF-Line"
              icon={<IconOffLine strokeColor="#A5B3C2"/>}
            />
          </Link>

          <Link to={"/daily-check-online-waiting-inspection"}>
            <DailyCheckTabButton
              className="on"
              tabTitle="ON - Line"
              totalNumbers="999+"
              icon={<IconONLine  strokeColor="#ffffff" />}
            />
          </Link>
        </DailyCheckTab>

        <DailyCommonRoundBox title="Check" state stateType="Success!">
          <PieChartCommon total={"50"} />

          <div className="legend-type">
            <span className="type01">
              <span className="bu"></span>My check<em className="number">23</em>
            </span>
            <span className="type02">
              <span className="bu"></span>Dept check
              <em className="number">15</em>
            </span>
          </div>
        </DailyCommonRoundBox>

        <div className="comm-fix-bottom div2">
          <Button outline>업로드</Button>
          <Link to={"/daily-check-waiting-inspection-list"}>
            <Button>점검시작</Button>
          </Link>
        </div>

        <MessageBar style={{bottom: "108px"}} messageText={"업로드 완료되었습니다!"} />
      </div>
    </>
  );
};

export default DailyCheckOnLineWaitingInspection;
