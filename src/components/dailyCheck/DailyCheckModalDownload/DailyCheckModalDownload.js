import React from "react";

import "./DailyCheckModalDownload.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

const DailyCheckModalDownload = ({style,onClick,onCancel}) => {
  return (
    <DailyCheckModal
      title="다운로드 대상"
      background="bg-type"
      position="modal-box-center"
      style={style}
    >
      <div className="modal-header">
        <h1>다운로드 대상</h1>
        <button className="btn-popup-close" onClick={onCancel}>
          <IconBase>
            <IconClosePopup />
          </IconBase>
        </button>
      </div>
      <div className="modal-contents">
        <p className="guide-text">다운로드받을 항목을 선택하세요</p>

        <div className="dc-download-state">
          <div className="dc-download-state-item">
            <input type="checkbox" id="DC-download-state01" />
            <label htmlFor="DC-download-state01">
              <span className="now">30</span>
              <span className="type">My Check</span>
              <span className="total">30건</span>
            </label>
          </div>

          <div className="dc-download-state-item">
            <input type="checkbox" id="DC-download-state02" />
            <label htmlFor="DC-download-state02">
              <span className="now">30</span>
              <span className="type">Dept Check</span>
              <span className="total">30건</span>
            </label>
          </div>
        </div>
      </div>

      <div className="btn-wrapper align-right">
        <Button size="small" color="text-only-gray" onClick={onCancel}>
          취소
        </Button>
        <Button size="small" color="text-only-primary" onClick={onClick}>
          확인
        </Button>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalDownload;
