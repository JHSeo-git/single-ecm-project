import React, { useState } from "react";
import cx from "classnames";

import DimedModal from "components/base/DimedModal";
import FixedFooter from "components/base/FixedFooter";

import { IconListSelect } from "components/common/Icons";
import Button from "components/common/Button";

import "./DailyCheckSelectBoxModal.scss";

const DailyCheckSelectBoxModal = ({ onClick, onSelect, style }) => {
  const [options, setOptions] = useState([
    {
      id: 1,
      text: "OK",
    },
    {
      id: 2,
      text: "NG",
    },
    {
      id: 3,
      text: "OK Uncheck",
    },
    {
      id: 4,
      text: "NG W/O",
    }
  ]);

  return (
    <DimedModal style={style}>
      <div className="work-order-select-wrapper">
        <div className="modal-header">
          <h1>Result</h1>
        </div>
        <div className="modal-select-list">
          <ul className="select-items">
            {options.map((option, idx) => (
              <li key={idx}>
                <div
                  data-id={option.id}
                  className={cx("select-item", idx === 0 && "active")}
                >
                  {option.text}
                  {idx === 0 && <IconListSelect />}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <FixedFooter className="select-box-button-wrapper">
        <Button fullWidth onClick={onClick}>
          확인
        </Button>
      </FixedFooter>
    </DimedModal>
  );
};

export default DailyCheckSelectBoxModal;
