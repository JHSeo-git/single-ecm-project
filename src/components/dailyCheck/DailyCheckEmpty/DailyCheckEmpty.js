import React from "react";

import "./DailyCheckEmpty.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import NoResult from "../../common/NoResult";

const DailyCheckEmpty = ({onClick}) => {
  return (
    <>
      <NoResult
        title1={"Oops! It’s Empty!"}
        subTitle1={"Please download your"}
        subTitle2={"Daily check list"}
        />
      <div className="common-btn">
        <Button onClick={onClick} style={{minWidth:"180px"}}>
          Download
        </Button>
      </div>
    </>
  );
};

export default DailyCheckEmpty;
