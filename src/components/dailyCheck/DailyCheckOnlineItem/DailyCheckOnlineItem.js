import React from "react";
import CheckBox from "../../common/CheckBox";
import RoundTag from "../../common/RoundTag";

import "./DailyCheckOnlineItem.scss";

const DailyCheckOnlineItem = ({
  value1,
  title,
  date,
  onChange,
  checked,
  point,
  tagName,
  roundTag,
  active,
  checkType,
  measure,
  description,
  ...rest
}) => {
  return (
    <div className="dc-online-list-item">
      <div className="dc-check-box">
        <CheckBox value1={value1} onChange={onChange} checked={checked} />
      </div>
      <div className="items-row-alt flex-sb">
        <div className="sp-col">
          <span className="title">{title}</span>
          <span className="date">{date}</span>
        </div>
        <div className="sp-col">
          <div className="rounded-point">{`${point} P`}</div>
        </div>
      </div>

      <div className="items-row-alt">
        <span className="item-description">{description}</span>
      </div>
      <div className="items-row-alt">
        {checkType && <div className="item-round-type">{checkType}</div>}
        <div className="item-round-type">{`Measure. ${measure}`}</div>
      </div>
    </div>
  );
};

export default DailyCheckOnlineItem;
