import React from "react";

import "./DailyCheckModalFail.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";
import FailLottie from "../../common/FailLottie";

const DailyCheckModalFail = ({style,onClick,onCancel}) => {
  return (
    <DailyCheckModal
      title="Fail"
      background="bg-type"
      position="modal-box-center"
      style={style}
    >
      <div className="modal-header">
        <h1>Fail!</h1>
        <button className="btn-popup-close" onClick={onCancel}>
          <IconBase>
            <IconClosePopup />
          </IconBase>
        </button>
      </div>
      <div className="modal-contents">
        <div className="notice-contents">
          <FailLottie />
          <div className="notice-text">
            26개의 데이터가<br />
            업로드에 실패하였습니다
          </div>
        </div>
      </div>

      <div className="btn-wrapper align-center">
        <Button size="small" color="primary" fullWidth onClick={onClick} style={{width:"170px"}}>
          확인
        </Button>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalFail;
