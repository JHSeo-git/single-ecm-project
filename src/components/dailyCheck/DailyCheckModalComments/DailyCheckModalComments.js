import React from "react";
import "./DailyCheckModalComments.scss";
import "components/dailyCheck/dc-common/DailyCheckCommon.scss";

import { IconClosePopup, IconComment } from "components/common/Icons";
import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

const DailyCheckModalComments = ({ style, onClick }) => {
  return (
    <DailyCheckModal
      title="Comments"
      background="bg-type"
      position="modal-fix-bottom"
      style={style}
    >
      <div className={"comments-modal-wrap"}>
        <div className="modal-header">
          <h1>
            <IconComment />
            Comments
          </h1>
          <button className="btn-popup-close" onClick={onClick}>
            <IconClosePopup />
          </button>
        </div>
        <div className={"comm-textarea"}>
          <textarea name="" id="" cols="30" rows="10"></textarea>
        </div>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalComments;
