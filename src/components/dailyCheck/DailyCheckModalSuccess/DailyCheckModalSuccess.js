import React from "react";

import "./DailyCheckModalSuccess.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";
import SuccessLottie from "../../common/SuccessLottie";

const DailyCheckModalSuccess = ({style,onClick,onCancel}) => {
  return (
    <DailyCheckModal
      title="Success"
      background="bg-type"
      position="modal-box-center"
      style={style}
    >
      <div className="modal-header">
        <h1>Success!</h1>
        <button className="btn-popup-close" onClick={onCancel}>
          <IconBase>
            <IconClosePopup />
          </IconBase>
        </button>
      </div>
      <div className="modal-contents">
        <div className="notice-contents">
          <SuccessLottie />
          <div className="notice-text">
            26개의 데이터가<br />
            성공적으로 업로드 되었습니다
          </div>
        </div>
      </div>

      <div className="btn-wrapper align-center">
        <Button size="small" color="primary" fullWidth onClick={onClick} style={{width:"170px"}}>
          확인
        </Button>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalSuccess;
