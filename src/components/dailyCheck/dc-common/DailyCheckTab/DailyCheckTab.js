import React from "react";
import classNames from 'classnames';
import "./DailyCheckTab.scss";

export const DailyCheckTabButton = ({
  className,
  icon,
  tabTitle,
  totalNumbers,
  onClick,
  ...rest
}) => {
  return (
    <button className={className} onClick={onClick}>
      {icon}
      <span className="title">{tabTitle}</span>
      <span className="total">{totalNumbers}</span>
    </button>
  );
};

const DailyCheckTab = ({ children, tabType }) => {
  return <div className={classNames("daily-tab", tabType)}>{children}</div>;
};

export default DailyCheckTab;
