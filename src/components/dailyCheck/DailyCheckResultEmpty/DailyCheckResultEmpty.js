import React from "react";

import "./DailyCheckResultEmpty.scss";
import "../dc-common/DailyCheckCommon.scss";

import Button from "components/common/Button";
import NoResult from "../../common/NoResult";

const DailyCheckResultEmpty = ({onClick}) => {
  const br = `\n`;
  return (
    <>
      <NoResult
        title1={"No Results!"}
        subTitle1={"해당 검새결과가 없습니다!"}
        subTitle2={"다시 시도해주세요."}
        />
      <div className="comm-fix-bottom">
        <Button fullWidth onClick={onClick}>
          점검항목 다운로드
        </Button>
      </div>
    </>
  );
};

export default DailyCheckResultEmpty;
