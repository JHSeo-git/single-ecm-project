import React from "react";

import "../dc-common/DailyCheckCommon.scss";
import "./DailyCheckModalVoiceRecording.scss";

import {
  IconClosePopup,
  IconMic,
  IconTrashCan,
  IconVoiceNext,
  IconVoicePlay,
  IconVoicePrev,
} from "components/common/Icons";

import DailyCheckModal from "components/dailyCheck/DailyCheckModal";

const DailyCheckModalVoiceRecording = ({ style, onClick }) => {
  return (
    <DailyCheckModal
      title="Voice memo"
      background="bg-type"
      position="modal-fix-bottom"
      style={style}
    >
      <div className="voice-recording-wrap">
        <div className="modal-header">
          <h1>
            <IconMic fill="#000000" />
            Voice memo
          </h1>
          <button className="btn-popup-close" onClick={onClick}>
            <IconClosePopup />
          </button>
        </div>

        <div className="voice-recording-content">
          <div className="voice-recording-item active">
            <div className="vr-header">
              <div>
                <span>음성녹음 2</span>
                <span>2020.09.10</span>
              </div>

              <div>48:10</div>
            </div>

            <div className="vr-contents">
              <div className="play-bar">
                <span className="recordline"></span>
                {/*임시*/}
                <span className="play-time">21:32</span>
              </div>

              <div className="play-btn">
                <div>
                  <button className="btn_prev10">
                    <IconVoicePrev />
                  </button>
                  <button className="btn_play">
                    <IconVoicePlay />
                  </button>
                  <button className="btn_next10">
                    <IconVoiceNext />
                  </button>
                </div>

                <button className="btn_delete">
                  <IconTrashCan />
                </button>
              </div>
            </div>
          </div>
          <div className="voice-recording-item ">
            <div className="vr-header">
              <div>
                <span>음성녹음 1</span>
                <span>2020.09.10</span>
              </div>

              <div>48:10</div>
            </div>

            <div className="vr-contents" style={{ display: "none" }}>
              <div className="play-bar">
                <span className="recordline"></span>
                {/*임시*/}
                <span className="play-time">21:32</span>
              </div>

              <div className="play-btn">
                <div>
                  <button className="btn_prev10">
                    <IconVoicePrev />
                  </button>
                  <button className="btn_play">
                    <IconVoicePlay />
                  </button>
                  <button className="btn_next10">
                    <IconVoiceNext />
                  </button>
                </div>

                <button className="btn_delete">
                  <IconTrashCan />
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="record-type">
          <button className="btn-record">
            <IconMic fill="#ffffff" />
          </button>
          <div className="new-record-bottom">
            <span className="title">음성녹음2</span>
            <span className="time">00:19:24</span>

            <span className="recordline"></span>
            {/*임시*/}
          </div>
          <button className="btn-record stop"></button>
        </div>
      </div>
    </DailyCheckModal>
  );
};

export default DailyCheckModalVoiceRecording;
