import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseMain from "components/base/BaseMain";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import DeliveryBarcode from "components/delivery/DeliveryBarcode";
import HeaderTitle from "components/common/HeaderTitle";
import {
  IconCommBack,
  IconBarcodeLine,
  IconMainReleaseFull,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import OKCancelModal from "components/modal/OKCancelModal";
import DimedModal from "components/base/DimedModal";

import DeliveryLPNCard from "components/delivery/DeliveryLPNCard";

import { useSnackbar } from "notistack";

import { testData } from "./testData";

import "./DeliveryLPN.scss";

const DeliveryLPN = ({ onClose }) => {
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const [items, setItems] = useState(testData);
  const [mode, setMode] = useState("");

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (inMessage, isOK = true) => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar(inMessage, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 108 }}
          onClick={() => closeSnackbar(key)}
        >
          <div className="ok-wrapper">
            {isOK && (
              <IconMainReleaseFull fill="#04B395" width="22" height="22" />
            )}
            <span className="text">{message}</span>
          </div>
        </div>
      ),
    });
  };

  const [testFlag, setTestFlag] = useState(false);
  const onSave = () => {
    if (!testFlag) {
      // error test
      setMode("VALIDATION_ERROR_MODAL");
      setTestFlag(true);
      return;
    }
    onShowMessage("정상 등록되었습니다");
    onClose();
  };
  return (
    <>
      <div className="delivery-lpn">
        <FixedHeader
          left={
            <IconBase onClick={onClose}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Select LPN" />}
        />
        <BaseMain
          className="delivery-lpn-main"
          style={{
            height: isKeyboardFocus
              ? `calc(100vh - 56px)`
              : `calc(100vh - (56px + 87px))`,
            marginTop: `56px`,
          }}
        >
          <div className="sticky-header">
            <h1 className="lpn-title">
              <span className="text">123675415</span>
            </h1>
            <p className="lpn-desc">
              DescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescriptionDescription
            </p>
            <div className="lpn-quantity-dashboard">
              <div className="dashboard-col">
                <span className="lpn-dashboard-label">Reserved Qty</span>
                <span className="lpn-primary">10</span>
              </div>
              <div className="splitter" />
              <div className="dashboard-col">
                <span className="lpn-dashboard-label">Sum Qty</span>
                <span className="lpn-primary">
                  {items.reduce((acc, current) => acc + current.quantity, 0)}
                </span>
              </div>
            </div>
          </div>
          {items && (
            <ul className="lpn-items">
              {items
                .sort((item) => item.itemId)
                .map((item) => (
                  <DeliveryLPNCard
                    key={item.itemId}
                    {...item}
                    inputMode={true}
                    onChangeQuantity={(e) => {
                      const targetId = item.itemId;
                      console.log("targetId : ", targetId);
                      console.log(
                        "val : ",
                        isNaN(parseInt(e.target.value))
                          ? 0
                          : parseInt(e.target.value)
                      );
                    }}
                    onFocus={() => {
                      setIsKeyboardFocus(true);
                    }}
                    onBlur={() => setIsKeyboardFocus(false)}
                  />
                ))}
            </ul>
          )}
        </BaseMain>
        {!isKeyboardFocus && (
          <FixedFooter className="footer-white">
            <div className="buttons">
              <button
                className="barcode-button"
                onClick={() => setMode("DELIVERY_DETAIL/BARCODE")}
              >
                <IconBarcodeLine />
              </button>
              <button className="save-button" onClick={onSave}>
                Save
              </button>
            </div>
          </FixedFooter>
        )}
      </div>
      <ModalFullPageAni
        modeState={mode}
        pageName="DELIVERY_DETAIL/BARCODE"
        motion="vertical"
      >
        <DeliveryBarcode
          onClose={() => setMode("")}
          deliveryId={"123456789"}
          onShowMessage={onShowMessage}
        />
      </ModalFullPageAni>
      {mode === "VALIDATION_ERROR_MODAL" && (
        <DimedModal className="modal-top-max return-confirm-modal">
          <OKCancelModal title="Validation Error" onConfirm={() => setMode("")}>
            <p className="modal-content">Validation Error 발생하였습니다.</p>
          </OKCancelModal>
        </DimedModal>
      )}
    </>
  );
};

export default DeliveryLPN;
