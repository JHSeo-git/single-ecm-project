import React from "react";
import cx from "classnames";
import { IconRoundClose, IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import "./DeliveryLPNCard.scss";

const DeliveryLPNCard = ({
  itemId,
  snNo,
  lpnNo,
  bin,
  quantity,
  quantityAvailable,
  inputMode = false,
  onChangeQuantity,
  isDelOn,
  isActive = false,
  onDelClick,
  onFocus = null,
  onBlur = null,
}) => {
  return (
    <li
      className={cx(
        "delivery-lpn-card",
        isDelOn && "del-mode",
        inputMode && "input-mode",
        isActive && "active-mode"
      )}
    >
      {isDelOn && (
        <IconBase className="del-icon" onClick={onDelClick}>
          <IconRoundClose />
        </IconBase>
      )}
      <div className="lpn-header">
        <div className="location">
          <IconLocation />
          <span className="text">{bin}</span>
        </div>
        <div className="snNo">{snNo}</div>
      </div>
      <div className="lpn-no">{lpnNo}</div>
      <div className="lpn-quantity">
        {inputMode ? (
          <>
            <span className="qa-label">Quantity Available</span>
            <span className="quantity">{quantityAvailable}</span>
            <div className="splitter" />
            <span className="qa-label">Quantity</span>
            <input
              type="number"
              className="quantity-input"
              value={quantity}
              onChange={onChangeQuantity}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          </>
        ) : (
          <>
            <span className="qa-label">Quantity</span>
            <span className="quantity">{quantity}</span>
          </>
        )}
      </div>
    </li>
  );
};

export default DeliveryLPNCard;
