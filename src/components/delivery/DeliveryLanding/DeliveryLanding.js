import React, { useState } from "react";
import cx from "classnames";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";

import DeliveryCardPanel from "components/delivery/DeliveryCardPanel";
import DeliveryDetail from "components/delivery/DeliveryDetail";

import { testData } from "./testData";

import {
  IconHamburger,
  IconDeliveryTabRequest,
  IconDeliveryTabSent,
  IconDeliveryTabComplete,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./DeliveryLanding.scss";

const DeliveryLanding = ({ onSearchClick }) => {
  const [mode, setMode] = useState("init");
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "owningDept", text: "QCA" },
    { id: "2", type: "fromto", text: "2020-08-11 ~ 2020-08-11" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);
  const [results] = useState(testData);

  const [prevScrollTop, setPrevScrollTop] = useState(0);
  const [hide, setHide] = useState(false);
  const handleScroll = (e) => {
    const scrollTop = e.currentTarget.scrollTop;
    const deltaY = scrollTop - prevScrollTop;
    const tmpHide = scrollTop !== 0 && deltaY >= 0;
    setHide(tmpHide);
    setPrevScrollTop(scrollTop);
  };

  // request / sent / complete
  const [tabMode, setTabMode] = useState("request");
  const [selectInfo, setSelectInfo] = useState(null);

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <div className="delivery-landing">
          <FixedHeader
            left={
              <IconBase
                onClick={
                  isMenuOpen ? undefined : () => setIsMenuOpen(!isMenuOpen)
                }
              >
                {!isMenuOpen && <IconHamburger />}
              </IconBase>
            }
            center={
              <div className="equipment-title">
                <HeaderTitle text="Delivery">
                  {results && <CircleText text={results.length} />}
                </HeaderTitle>
              </div>
            }
          />

          <BaseMain
            className="delivery-main"
            isMenuOpen={isMenuOpen}
            onMenuClick={() => setIsMenuOpen(!isMenuOpen)}
            style={{
              height: `calc(100vh - (56px + 60px + 55px))`,
              marginTop: `calc(56px + 55px)`,
            }}
            onScroll={handleScroll}
          >
            <FixedWrapper style={{ top: "56px" }}>
              <SearchConditionPanelTypeB
                onSearchClick={onSearchClick}
                keywords={keywords}
                setKeywords={setKeywords}
                isBorderBot={false}
              />
            </FixedWrapper>
            <div className={cx("delivery-header", !hide && "sticky-header")}>
              <div className="inner">
                <div
                  className={cx(
                    "delivery-tab",
                    tabMode === "request" && "active"
                  )}
                  onClick={() => setTabMode("request")}
                >
                  <IconDeliveryTabRequest width="30" height="30" />
                  <span className="tab-text">Request</span>
                </div>
                <div
                  className={cx("delivery-tab", tabMode === "sent" && "active")}
                  onClick={() => setTabMode("sent")}
                >
                  <IconDeliveryTabSent width="30" height="30" />
                  <span className="tab-text">Sent</span>
                </div>
                <div
                  className={cx(
                    "delivery-tab",
                    tabMode === "complete" && "active"
                  )}
                  onClick={() => setTabMode("complete")}
                >
                  <IconDeliveryTabComplete width="30" height="30" />
                  <span className="tab-text">Complete</span>
                </div>
                <div className={cx("animation-bg", tabMode)} />
              </div>
            </div>
            {tabMode === "request" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "request")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
            {tabMode === "sent" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "sent")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
            {tabMode === "complete" && (
              <ul
                className="delivery-items"
                style={{ marginTop: `calc(10px + ${!hide ? "59px" : "0px"})` }}
              >
                {results
                  .filter((item) => item.deliveryState === "complete")
                  .map((item) => (
                    <li key={item.deliveryId} className="delivery-item">
                      <DeliveryCardPanel
                        onClick={() => {
                          setMode("DELIVERY_DETAIL");
                          setSelectInfo(item);
                        }}
                        {...item}
                      />
                    </li>
                  ))}
              </ul>
            )}
          </BaseMain>
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        </div>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="DELIVERY_DETAIL">
        <DeliveryDetail onBackClick={() => setMode("")} {...selectInfo} />
      </ModalFullPageAni>
    </>
  );
};

export default DeliveryLanding;
