import React, { useRef, useState, useEffect } from "react";
import SignaturePad from "react-signature-canvas";

import DimedModal from "components/base/DimedModal";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./DeliverySignature.scss";

const DeliverySignature = ({ onClose, setSign }) => {
  const signPad = useRef(null);

  const onClick = () => {
    setSign(signPad.current.getCanvas().toDataURL("image/png"));
    onClose();
  };

  const [canvasWidth, setCanvasWidth] = useState(0);
  const [canvasHeight, setCanvasHeight] = useState(0);

  useEffect(() => {
    if (signPad?.current?.getCanvas()) {
      setCanvasWidth(signPad.current.getCanvas().offsetWidth);
      setCanvasHeight(signPad.current.getCanvas().offsetHeight);
    }
  }, []);

  const onCheck = () => {
    console.log(signPad.current.getCanvas().offsetHeight);
    console.log(signPad.current.getCanvas().offsetWidth);
  };
  return (
    <DimedModal className="delivery-signature">
      <div className="signature-wrapper">
        <div className="signature-header">
          <span className="title" onClick={onCheck}>
            Signature
          </span>
          <IconBase className="close-icon" onClick={onClose}>
            <IconClosePopup />
          </IconBase>
        </div>
        <SignaturePad
          penColor="black"
          canvasProps={{
            className: "sign-pad",
            width: canvasWidth,
            height: canvasHeight,
          }}
          velocityFilterWeight={0.5}
          ref={signPad}
        />
        <div className="signature-button" onClick={onClick}>
          <span className="save-text">Save</span>
        </div>
      </div>
    </DimedModal>
  );
};

export default DeliverySignature;
