import React from "react";

import "./LabelDivTypeA.scss";

const LabelDivTypeA = ({ label, value }) => {
  return (
    <div className="label-div-typeA">
      <span className="info-label">{label}</span>
      <span className="info-value">{value}</span>
    </div>
  );
};

export default LabelDivTypeA;
