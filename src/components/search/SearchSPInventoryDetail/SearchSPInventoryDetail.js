import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";

import { IconCommBack } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./SearchSPInventoryDetail.scss";

const SearchSPInventoryDetail = ({ spId, onClose }) => {
  const [targetSpId, setTargetSpId] = useState(spId);
  const [results, setResults] = useState([
    {
      bin: "central",
      lotId: "000000000000",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000001",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000002",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000003",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000004",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000005",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000006",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000007",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000008",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000009",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
    {
      bin: "central",
      lotId: "000000000010",
      conditionCode: "New",
      quantity: "0",
      quantityAvailable: "992",
    },
  ]);

  const onSearchFormBack = () => {
    onClose();
  };

  return (
    <div className="search-sp-inventory-detail">
      <FixedHeader
        left={
          <IconBase onClick={onSearchFormBack}>
            <IconCommBack />
          </IconBase>
        }
        center={<HeaderTitle text={targetSpId} />}
      />
      <main className="search-sp-inventory-detail-main">
        <table className="result-table">
          <thead>
            <tr>
              <th>Bin</th>
              <th>Lot</th>
              <th>Condition Code</th>
              <th>Quantity</th>
              <th>Quantity Available</th>
            </tr>
          </thead>
          <tbody>
            {results &&
              results.map((result, idx) => (
                <tr key={idx}>
                  <td>{result.bin}</td>
                  <td>{result.lotId}</td>
                  <td>{result.conditionCode}</td>
                  <td>{result.quantity}</td>
                  <td>{result.quantityAvailable}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </main>
    </div>
  );
};

export default SearchSPInventoryDetail;
