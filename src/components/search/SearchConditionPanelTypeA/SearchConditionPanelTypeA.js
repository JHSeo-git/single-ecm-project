import React from "react";
import { Link } from "react-router-dom";
import cx from "classnames";

import SearchKeywordsPanel from "components/search/SearchKeywordsPanel";

import { IconSearchSmall } from "components/common/Icons";

import "./SearchConditionPanelTypeA.scss";

const SearchConditionPanelTypeA = ({
  to,
  keywords,
  setKeywords,
  searchType,
  isBorderBot = true,
  ...rest
}) => {
  return (
    <div
      className={cx("search-panel-typeA", !isBorderBot && "none-bord")}
      {...rest}
    >
      <div className="search-panel-overflow">
        <SearchKeywordsPanel
          keywords={keywords}
          setKeywords={setKeywords}
          searchType={searchType}
        />
      </div>
      {to && (
        <Link to={to} className="search-icon">
          <IconSearchSmall />
        </Link>
      )}
    </div>
  );
};

export default SearchConditionPanelTypeA;
