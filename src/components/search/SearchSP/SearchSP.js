import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import ScrollableModal from "components/base/ScrollableModal";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";
import NoResult from "components/common/NoResult";

import SearchConditionPanelTypeA from "components/search/SearchConditionPanelTypeA";
import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import SearchResultHeaderTypeA from "components/search/SearchResultHeaderTypeA";
import SearchResultCardTypeA from "components/search/SearchResultCardTypeA";
import LabelDivTypeA from "components/search/LabelDivTypeA";
import LabelDivTypeB from "components/search/LabelDivTypeB";
import SearchResultItemTypeA from "components/search/SearchResultItemTypeA";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";
import ModalItemRow from "components/common/ModalItemRow";

import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./SearchSP.scss";

const SearchSP = ({ onSearchClick }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [keywords, setKeywords] = useState([
    { id: "1", type: "codification", text: "조건1" },
    { id: "2", type: "codification", text: "조건2" },
    { id: "7", type: "itemCode", text: "123444" },
    { id: "8", type: "itemCode", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);
  const [results, setResults] = useState([
    {
      itemCode: "BMBOLT0001",
      codification: "BMACT",
      description: "Actuator, Fettle, Premium linear...",
      maker: "FETTLE",
    },
    {
      itemCode: "BMBOLT0002",
      codification: "BMACT",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "GDANE",
    },
    {
      itemCode: "BMBOLT0003",
      codification: "BCGAS",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "SKF",
    },
    {
      itemCode: "BMBOLT0004",
      codification: "BCGAS",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "MOBIL",
    },
    {
      itemCode: "BMBOLT0005",
      codification: "BMACT",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "IHC",
    },
    {
      itemCode: "BMBOLT0006",
      codification: "BMBOL",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "IHC",
    },
    {
      itemCode: "BMBOLT0007",
      codification: "BMBOL",
      description: "Bolt, Mobile, A109K-4000H",
      maker: "IHC",
    },
  ]);

  const [selectedIdx, setSelectedIdx] = useState(-1);

  const onMenuClick = (e) => {
    setIsMenuOpen(!isMenuOpen);
  };

  const onSelect = (e, idx) => {
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;

    if (idx === selectedIdx) {
      setSelectedIdx(-1);
    } else {
      setSelectedIdx(idx);
    }
  };

  const onCancelSelect = () => {
    setSelectedIdx(-1);
  };

  return (
    <div className="search-sp">
      <FixedHeader
        left={
          <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
            {!isMenuOpen && <IconHamburger />}
          </IconBase>
        }
        center={
          <HeaderTitle text="Spare Parts">
            {results && <CircleText text={results.length} />}
          </HeaderTitle>
        }
      />
      <BaseMain
        isMenuOpen={isMenuOpen}
        isModalOpen={selectedIdx > -1}
        onMenuClick={onMenuClick}
        style={{
          height: `calc(100vh - (56px + 57px + 56px + 60px))`,
          marginTop: `calc(56px + 57px + 56px)`,
        }}
      >
        <FixedWrapper style={{ top: "56px" }}>
          <SearchConditionPanelTypeB
            onSearchClick={onSearchClick}
            keywords={keywords}
            setKeywords={setKeywords}
          />
          <SearchResultHeaderTypeA
            label1="Item code"
            label2="Codification"
            label3="Description"
            label4="Maker"
          />
        </FixedWrapper>
        <div className="result-panel">
          {results?.length === 0 ? (
            <NoResult title1="No Result!" subTitle1="항목이 없습니다!" />
          ) : (
            <ul className="result-items">
              {results.map((result, idx) => (
                <li key={idx}>
                  <SearchResultItemTypeA
                    isActive={idx === selectedIdx}
                    data-id={result.equipmentId}
                    onClick={(e) => onSelect(e, idx)}
                  >
                    <SearchResultCardTypeA
                      value1={result.itemCode}
                      value2={result.codification}
                      value3={result.description}
                      value4={result.maker}
                    />
                  </SearchResultItemTypeA>
                </li>
              ))}
            </ul>
          )}
        </div>
      </BaseMain>
      {selectedIdx > -1 && (
        <ScrollableModal onTouchHandler={onCancelSelect}>
          <ModalItemRow>
            <LabelDivTypeA
              label="Description"
              value="Package-챔버-01-ABSDDGDD"
            />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeA label="Material Group" value="40142000" />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeA label="Model" value="Premium Linear Actuators" />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeA
              label="Spec"
              value="Stroke 1 to 30, Ranging From 35- 200"
            />
          </ModalItemRow>
          <ModalItemRow>
            <LabelDivTypeB label="Discontinued Flag" value="Y" />
          </ModalItemRow>
        </ScrollableModal>
      )}
      <FixedFooter>
        <AppNavigation />
      </FixedFooter>
    </div>
  );
};

export default SearchSP;
