import React from "react";

import { IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SearchKeywordsResultSerialNo.scss";

const SearchKeywordsResultSerialNo = ({
  id,
  location,
  title,
  serialStatus,
  onClick,
  ...rest
}) => {
  return (
    <div
      className="search-keyword-result-serial-no"
      onClick={onClick}
      {...rest}
    >
      <div className="row">
        <div className="item-title">{title}</div>
      </div>
      <div className="row">
        <div className="icon-with-text">
          <IconBase className="location-icon">
            <IconLocation width="16" height="16" />
          </IconBase>
          <span className="location-text">{location}</span>
        </div>
        <div className="item-rounded-text">{serialStatus}</div>
      </div>
    </div>
  );
};

export default SearchKeywordsResultSerialNo;
