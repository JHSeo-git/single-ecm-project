import React from "react";

import { IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SearchKeywordsResultSpReqItem.scss";

// id: "1023",
// type: "spIssueResult",
// text: "BMBOLT0003",
// location: "BF 1",
// description: "Bolt, Mobile, A109K-4000H",
// quantityAvailable: "992",

const SearchKeywordsResultSpReqItem = ({
  id,
  location,
  description,
  title,
  subText,
  onClick,
  ...rest
}) => {
  return (
    <div
      className="search-keyword-result-sp-req-item"
      onClick={onClick}
      {...rest}
    >
      <div className="row">
        <div className="item-title">{title}</div>
        <div className="icon-with-text">
          <IconBase className="location-icon">
            <IconLocation width="16" height="16" />
          </IconBase>
          <span className="location-text">{location}</span>
        </div>
      </div>
      <div className="row">
        <div className="item-desc">{description}</div>
      </div>
      <div className="row">
        <div className="item-label">Quantity Available</div>
        <div className="item-value">{subText}</div>
      </div>
    </div>
  );
};

export default SearchKeywordsResultSpReqItem;
