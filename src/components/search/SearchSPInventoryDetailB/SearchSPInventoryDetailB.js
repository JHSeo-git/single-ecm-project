import React, { useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import SearchSPInventoryDetailItemCard from "components/search/SearchSPInventoryDetailItemCard";

import { IconCommBack, IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";
import CircleText from "components/common/CircleText";
import SwitchButton from "components/common/SwitchButton";

import thumbnailImg from "asset/images/inventory-default-thumbnail.png";

import "./SearchSPInventoryDetailB.scss";

const SearchSPInventoryDetailB = ({ spId, onClose }) => {
  const [isOn, setIsOn] = useState(false);
  const onSwitchClick = () => {
    setIsOn(!isOn);
  };
  const [targetSpId, setTargetSpId] = useState(spId);
  const [results, setResults] = useState({
    itemInfo: {
      itemCode: "BMBOLT000111111111111",
      storeroom: "centralcentralcentral",
      description: "Actuator, Fettle, Premium linear",
      quantity: "000",
      quantityAvailable: "992",
    },
    data: [
      {
        bin: "central",
        lotId: "000000000000",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000001",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000002",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000003",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000004",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000005",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000006",
        conditionCode: "New",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000007",
        conditionCode: "My",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000008",
        conditionCode: "My",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000009",
        conditionCode: "My",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
      {
        bin: "central",
        lotId: "000000000010",
        conditionCode: "My",
        quantity: "0",
        quantityAvailable: "992",
        location: "A1-1-2",
      },
    ],
  });

  const onSearchFormBack = () => {
    onClose();
  };

  return (
    <div className="search-sp-inventory-detail-b">
      <FixedHeader
        left={
          <IconBase onClick={onSearchFormBack}>
            <IconCommBack />
          </IconBase>
        }
        center={
          <HeaderTitle text={targetSpId}>
            {results?.data && <CircleText text={results.data.length} />}
          </HeaderTitle>
        }
      />
      <main className="search-sp-inventory-detail-b-main">
        <div className="item-title-wrapper">
          <div className="item-title-info">
            <div className="col item-picture">
              <img src={thumbnailImg} alt="item thumbnail" />
            </div>
            <div className="col">
              <div className="item-code">
                <span>{results.itemInfo.itemCode}</span>
              </div>
              <div className="item-storeroom">
                <IconBase>
                  <IconLocation width="16" height="16" fill="#E0205C" />
                </IconBase>
                <span>{results.itemInfo.storeroom}</span>
              </div>
              <div className="item-desc">
                <span>{results.itemInfo.description}</span>
              </div>
              <div className="item-quantity">
                <span className="label">Quantity</span>
                <span className="value">{results.itemInfo.quantity}</span>
              </div>
              <div className="item-quantity">
                <span className="label">Quantity Available</span>
                <span className="value">
                  {results.itemInfo.quantityAvailable}
                </span>
              </div>
            </div>
          </div>
          <div className="filter-col">
            <span className="filter-label">My</span>
            <SwitchButton
              isOn={isOn}
              setIsOn={setIsOn}
              onClick={onSwitchClick}
            />
          </div>
        </div>
        {results?.data?.length > 0 && (
          <ul className="detail-items">
            {results?.data
              ?.filter((item) => (isOn ? item.conditionCode === "My" : true))
              .map((item, idx) => (
                <li key={idx}>
                  <SearchSPInventoryDetailItemCard {...item} />
                </li>
              ))}
          </ul>
        )}
      </main>
    </div>
  );
};

export default SearchSPInventoryDetailB;
