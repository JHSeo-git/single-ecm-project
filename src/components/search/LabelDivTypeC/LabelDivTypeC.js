import React from "react";

import { IconLink } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./LabelDivTypeC.scss";

const LabelDivTypeC = ({ label, value }) => {
  return (
    <div className="label-div-typeC">
      <span className="info-add-box-label">{label}</span>
      <div className="ic-link-box">
        {/* TODO: Add <Link> maybe */}
        <IconBase>
          <IconLink />
        </IconBase>
      </div>
    </div>
  );
};

export default LabelDivTypeC;
