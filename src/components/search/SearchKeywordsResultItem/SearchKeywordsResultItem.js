import React from "react";
import cx from "classnames";

import "./SearchKeywordsResultItem.scss";

const SearchKeywordsResultItem = ({
  id,
  text,
  subText,
  subId,
  isExpandMode = false,
  isFlex = false,
  ...rest
}) => {
  return (
    <>
      {isExpandMode ? (
        <div
          className={cx(
            "search-keywords-result-item-expand-mode",
            isFlex && "flex-box"
          )}
          {...rest}
        >
          {/* <span className="item-id">{id}</span> */}
          <div className="row">
            <span className="item-text">{text}</span>
            {/* <span className="box-text">{subId}</span> */}
          </div>
          <div className="row">
            <span className="sub-text">{subText}</span>
          </div>
        </div>
      ) : (
        <div className="search-keywords-result-item" {...rest}>
          <span className="item-id">{id}</span>
          <span className="item-text">{text}</span>
        </div>
      )}
    </>
  );
};

export default SearchKeywordsResultItem;
