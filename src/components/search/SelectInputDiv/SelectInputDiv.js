import React from "react";
import cx from "classnames";

import { IconSelectBoxArrow } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SelectInputDiv.scss";

// interface Props {
//   labelText?: string;
//   onClick: () => void;
//   placeholder: string;
//   beforeIcon: React.ReactNode;
//   value: React.ReactNode;
// }

const SelectInputDiv = ({
  labelText,
  onClick,
  placeholder = "Placeholder",
  beforeIcon,
  value,
  className,
}) => {
  return (
    <div className="select-input-div">
      <label>{labelText}</label>
      <div className={cx("select-input-box", className)} onClick={onClick}>
        <div className={cx("value", !value && "placeholder")}>
          {beforeIcon}
          {value ? value : placeholder}
        </div>
        <IconBase>
          <IconSelectBoxArrow />
        </IconBase>
      </div>
    </div>
  );
};

export default SelectInputDiv;
