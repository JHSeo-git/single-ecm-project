import React, { useEffect, useState } from "react";

import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import FixedFooter from "components/base/FixedFooter";
import BaseFormDetailMain from "components/base/BaseFormDetailMain";

import { IconCommBack, IconSearchBig } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import SearchDetailHeaderTypeA from "components/search/SearchDetailHeaderTypeA";
import SearchKeywordsResultItem from "components/search/SearchKeywordsResultItem";
import SearchConditionPanelTypeA from "components/search/SearchConditionPanelTypeA";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";

//test
import { testData } from "lib/api/test";

import "./FormDetail.scss";

const FormDetail = ({ type, onClose, initValue = "" }) => {
  const [keywords, setKeywords] = useState([
    { id: "1", type: "owningDept", text: "조립1팀" },
    { id: "2", type: "owningDept", text: "조립2팀" },
    { id: "3", type: "owningDept", text: "조립3팀" },
    { id: "4", type: "owningDept", text: "조립4팀" },
    { id: "5", type: "owningDept", text: "조립5팀" },
    { id: "6", type: "owningDept", text: "조립6팀" },
    { id: "7", type: "equipmentId", text: "123444" },
    { id: "8", type: "equipmentId", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);

  const [testApiData, setTestApiData] = useState(testData);

  const [searchKeywords, setSearchKeywords] = useState("");
  const [searchInputValue, setSearchInputValue] = useState(initValue);
  const [searchType, setSearchType] = useState();
  const [searchTerm, setSearchTerm] = useState("");

  const onSearchFormBack = () => {
    // hide page
    onClose();
  };
  const onSearchConfirm = () => {
    // TODO: add search conditions(searchKeywords => keywords)
    // hide page
  };

  const onEnterKeyPress = (e) => {
    if (e.key === "Enter") {
      setSearchTerm(searchInputValue);
    }
  };

  const onSearch = () => {
    setSearchTerm(searchInputValue);
  };

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    setSearchInputValue(value);
  };

  const onAddClick = (e) => {
    const { currentTarget } = e;
    const {
      dataset: { id },
    } = currentTarget;

    setSearchKeywords([
      ...searchKeywords,
      ...testApiData.filter((item) => item.id === id),
    ]);
  };

  const onInputAllClearClick = () => {
    setSearchKeywords([]);
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const onBlur = () => {
    setIsKeyboardFocus(false);
  };
  const onFocus = () => {
    setIsKeyboardFocus(true);
  };

  const initialize = () => {
    setSearchType(type);

    setSearchKeywords(keywords.filter((keyword) => keyword.type === type));

    if (initValue) {
      onSearch();
    }
  };

  useEffect(() => {
    initialize();
  }, []);

  return (
    <div className="search-form">
      <FixedHeader
        left={
          <IconBase onClick={onSearchFormBack}>
            <IconCommBack />
          </IconBase>
        }
        center={
          keywords && (
            <SearchDetailHeaderTypeA
              searchInputValue={searchInputValue}
              onChange={onChange}
              onEnterKeyPress={onEnterKeyPress}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          )
        }
        right={
          <IconBase onClick={onSearch}>
            <IconSearchBig />
          </IconBase>
        }
        centerScrollX={true}
      />
      <BaseFormDetailMain
        isAddedPanel={searchKeywords?.length > 0}
        style={{
          height:
            searchKeywords?.length > 0
              ? "calc(100vh - (56px + 56px + 79px))"
              : "calc(100vh - (56px + 79px))",
        }}
      >
        {searchKeywords?.length > 0 && (
          <FixedWrapper style={{ top: "56px" }}>
            <SearchConditionPanelTypeA
              keywords={searchKeywords}
              setKeywords={setSearchKeywords}
              searchType={searchType}
              isBorderBot={false}
            />
          </FixedWrapper>
        )}
        {searchTerm && (
          <ul>
            {testApiData
              ?.filter(
                (item) =>
                  item.type === searchType && item.text.indexOf(searchTerm) > -1
              )
              .map((item, idx) => (
                <li key={idx}>
                  <SearchKeywordsResultItem
                    id={item.id}
                    text={item.text}
                    data-id={item.id}
                    onClick={onAddClick}
                  />
                </li>
              ))}
          </ul>
        )}
      </BaseFormDetailMain>
      {!isKeyboardFocus && (
        <FixedFooter>
          <SearchButtonPanelTypeA
            onInputAllClearClick={onInputAllClearClick}
            clearNumber={searchKeywords?.length > 0 && searchKeywords?.length}
            btnColor="gray"
            btnText="확인"
            onClick={onSearchConfirm}
          />
        </FixedFooter>
      )}
    </div>
  );
};

export default FormDetail;
