import React from "react";

import { IconLocation } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SearchSPInventoryDetailItemCard.scss";

const SearchSPInventoryDetailItemCard = ({
  bin,
  lotId,
  conditionCode,
  quantity,
  quantityAvailable,
  location,
}) => {
  return (
    <div className="search-sp-inventory-detail-item-card">
      <div className="row-flex">
        <div className="col-sb">
          {/* <span className="label">Condition Code</span>
          <span className="value">{conditionCode}</span> */}
          <div className="condition-code">{conditionCode}</div>
          <div className="location-code">
            <IconBase className="icon-location">
              <IconLocation fill="#E0205C" width="16" height="16" />
            </IconBase>
            <span>{location}</span>
          </div>
        </div>
      </div>
      <div className="row-flex">
        <div className="col-sb">
          <span className="label">Quantity</span>
          <span className="value">{quantity}</span>
        </div>
        <div className="splitter" />
        <div className="col-sb">
          <span className="label">Quantity Available</span>
          <span className="value">{quantityAvailable}</span>
        </div>
      </div>
    </div>
  );
};

export default SearchSPInventoryDetailItemCard;
