import React from "react";
import "./SearchResultCardTypeA.scss";

const SearchResultCardTypeA = ({ value1, value2, value3, value4 }) => {
  return (
    <div className="search-result-card-typeA">
      <div className="item-row">
        <span className="value1">{value1}</span>
        <span className="value2">{value2}</span>
      </div>
      <div className="item-row">
        <span className="value3">{value3}</span>
        <span className="value4">{value4}</span>
      </div>
    </div>
  );
};

export default SearchResultCardTypeA;
