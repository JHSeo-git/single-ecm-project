import React from "react";
import cx from "classnames";

import "./BaseFormDetailMain.scss";

const BaseFormDetailMain = ({ isAddedPanel, children, ...rest }) => {
  return (
    <main
      className={cx("search-form-detail-main", isAddedPanel && "added-panel")}
      {...rest}
    >
      {children}
    </main>
  );
};

export default BaseFormDetailMain;
