import React from "react";

import "./BaseFormMain.scss";

const BaseFormMain = ({ children, ...rest }) => {
  return (
    <main className="search-form-main" {...rest}>
      {children}
    </main>
  );
};

export default BaseFormMain;
