import React from "react";
import cx from "classnames";
import "./IconBase.scss";

const IconBase = ({ children, className, ...rest }) => {
  return (
    <div className={cx("base", "icon-box", className)} {...rest}>
      {children}
    </div>
  );
};

export default IconBase;
