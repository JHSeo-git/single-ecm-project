import React from "react";
import { IconCounterMinus, IconCounterPlus } from "components/common/Icons";
import classNames from "classnames";
import "./CounterInput.scss";

const CounterInput = ({
  value,
  setValue,
  onIncrease,
  onDecrease,
  disabled = false,
  className,
}) => {
  const onChange = (e) => {
    console.log(e.target.value, value);
    if (!e?.target?.value) {
      setValue("");
      return;
    }
    setValue(parseInt(e.target.value.replace(/^[0]+/g, ""), 10));
  };
  return (
    <div className={classNames("counter-input typeWide", disabled, className)}>
      <button onClick={onDecrease}>
        <IconCounterMinus />
      </button>
      <input
        type="number"
        className="count-number"
        value={Number(value).toString()}
        onChange={onChange}
        placeholder="0"
      />
      <button onClick={onIncrease}>
        <IconCounterPlus />
      </button>
    </div>
  );
};

export default CounterInput;
