import React from "react";
import classNames from "classnames";

import "./MessageBar.scss";

const MessageBar = ({ className, style,fullWidth, messageText,iconType,refresh }) => {
  return (
    <div className={classNames("messages-bar", {fullWidth})} style={style}>
      {iconType}
      {messageText}
      {refresh}
    </div>
  );
};

export default MessageBar;
