import React from "react";
import classNames from "classnames";

const Button = ({
  children,
  size,
  color,
  outline,
  fullWidth,
  className,
  disabled,
  onClick,
  style,
  ...rest
}) => {
  return (
    <button
      className={classNames("btn", size, color, {
        outline,
        fullWidth,
      })}
      style={style}
      onClick={onClick}
      disabled={disabled}
      {...rest}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  size: "large",
  color: "gray",
  disabled:""
};

export default Button;
