import React from "react";
import cx from "classnames";

import { IconClosePopup, IconListSelect } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./SingleSelect.scss";

// interface Item {
//     id: string;
//     value: string;
// }

// interface Props {
//     title: string;
//     items: Item[];
//     onClose: () => void;
//     onSelect: (e: React.MouseEvent<HTMLElement>) => void;
//     selectedId: string;
// }

const SingleSelect = ({ title, onClose, onSelect, items, selectedId }) => {
  return (
    <div className="single-select-wrapper">
      <div className="select-header">
        <h3 className="header-text">{title}</h3>
        <IconBase onClick={onClose}>
          <IconClosePopup />
        </IconBase>
      </div>
      <ul className="select-items">
        {items?.map((item, idx) => (
          <li
            className="select-item"
            key={item.id ? item.id : idx}
            id={item.id}
            onClick={onSelect}
          >
            <div
              className={cx(
                "select-item-content",
                item.id && item.id === selectedId && "active"
              )}
            >
              {item.value}
            </div>
            {item.id && item.id === selectedId && (
              <IconBase className="selector-icon">
                <IconListSelect />
              </IconBase>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SingleSelect;
