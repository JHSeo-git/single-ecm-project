import React, { useState } from "react";
import cx from "classnames";

import { IconClosePopup, IconListSelect } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./MultiSelect.scss";

const SelectItem = ({ onSelect, className, isSelected, children, ...rest }) => {
  return (
    <li className={cx("select-item", className)} onClick={onSelect} {...rest}>
      <div className={cx("select-item-content", isSelected && "active")}>
        {children}
      </div>
      {isSelected && (
        <IconBase className="selector-icon">
          <IconListSelect />
        </IconBase>
      )}
    </li>
  );
};

// interface Item {
//     id: string;
//     value: string;
// }

// interface Props {
//     title: string;
//     items: Item[];
//     onClose: () => void;
//     onSelect: (e: React.MouseEvent<HTMLElement>) => void;
//     onConfirm: () => void;
//     selectedIds: string[];
// }

const MultiSelect = ({
  title,
  onClose,
  items,
  selectedIds,
  setSelectedIds,
}) => {
  const [tempSelectedIds, setTempSelectedIds] = useState(selectedIds);

  const onSelect = (e) => {
    if (!e?.target?.id) return;
    if (tempSelectedIds.includes(e.target.id)) {
      setTempSelectedIds(tempSelectedIds.filter((id) => id !== e.target.id));
    } else {
      setTempSelectedIds([...tempSelectedIds, e.target.id]);
    }
  };

  const onConfirm = () => {
    setSelectedIds(tempSelectedIds);
    onClose();
  };
  return (
    <div className="multi-select-wrapper">
      <div className="select-header">
        <h3 className="header-text">{title}</h3>
        <IconBase onClick={onClose}>
          <IconClosePopup />
        </IconBase>
      </div>
      <ul className="select-items">
        {items?.map((item, idx) => (
          <SelectItem
            key={item.id ? item.id : idx}
            id={item.id}
            onSelect={onSelect}
            isSelected={tempSelectedIds.includes(item.id)}
          >
            {item.value}
          </SelectItem>
        ))}
      </ul>
      <div className="select-button" onClick={onConfirm}>
        <span className="button-text">확인</span>
      </div>
    </div>
  );
};

export default MultiSelect;
