import React from "react";
import { Link, withRouter } from "react-router-dom";
import cx from "classnames";

import IconBase from "components/common/IconBase";
import {
  IconHomeRegular,
  IconHome,
  IconAlarm,
  IconAlarmSolid,
  IconUpload,
} from "components/common/Icons";

import "./AppNavigation.scss";

const AppNavigation = withRouter(({ location }) => {
  const { pathname } = location;
  return (
    <nav className="app-nav">
      <ul>
        <li>
          <Link
            to="/alarm"
            className={cx("icon", pathname === "/alarm" && "selected")}
          >
            {pathname === "/alarm" ? <IconAlarmSolid /> : <IconAlarm />}

            <span>Alarm</span>
          </Link>
        </li>
        <li>
          <Link
            to="/"
            className={cx(
              "icon",
              pathname !== "/alarm" && pathname !== "/setting" && "selected"
            )}
          >
            {pathname !== "/alarm" && pathname !== "/setting" ? (
              <IconHome />
            ) : (
              <IconHomeRegular />
            )}

            <span>Home</span>
          </Link>
        </li>
        <li>
          <Link
            to="/logout"
            className={cx("icon", pathname === "/logout" && "selected")}
          >
            <IconBase className="icon-logout">
              <IconUpload />
            </IconBase>
            <span>Logout</span>
          </Link>
        </li>
        {/* <li>
          <Link
            to="/setting"
            className={cx("icon", pathname === "/setting" && "selected")}
          >
            {pathname === "/setting" ? <IconGearSolid /> : <IconGear />}

            <span>Setting</span>
          </Link>
        </li> */}
      </ul>
    </nav>
  );
});

export default AppNavigation;
