import React from "react";
import cx from "classnames";

import { IconGrayPolygon, IconPersonPolygon } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./ActualStateDisplay.scss";

const ActualStateDisplay = ({ isStarted, className }) => {
  return (
    <div className={cx("actual-state-display", className)}>
      <div className={cx("rectangle", isStarted && "processing")}>
        {isStarted ? (
          <IconBase className="circle">
            <IconPersonPolygon />
          </IconBase>
        ) : (
          <IconGrayPolygon />
        )}
      </div>
    </div>
  );
};

export default ActualStateDisplay;
