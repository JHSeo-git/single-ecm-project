import React, { useState } from "react";
import classnames from "classnames";
import "./SwitchButton.scss";

const SwitchButton = ({ isOn, setIsOn, onClick }) => {
  return (
    <div
      className={classnames("switch-btn", isOn ? "on" : "off")}
      onClick={onClick}
    >
      <span className="round-handle">&nbsp;</span>
    </div>
  );
};

export default SwitchButton;
