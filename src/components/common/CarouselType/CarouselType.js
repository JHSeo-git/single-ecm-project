import React from 'react';
import Carousel from 'react-elastic-carousel';
import './CarouselType.scss'
const Item = ({children}) => {
  return (
    <div>{children}</div>
  )
}
const ItemContents = () => {
  return (
    <div className={"item-contents"}>
      점검결과
    </div>
  )
}
const CarouselType = () => {
  return (
    <div className={"carousel-wrap"}>
      <Carousel itemsToShow={1.1}>
        <Item><ItemContents/></Item>
        <Item><ItemContents/></Item>
        <Item><ItemContents/></Item>
        <Item><ItemContents/></Item>
        <Item><ItemContents/></Item>
        <Item><ItemContents/></Item>
      </Carousel>
    </div>
  );
};

export default CarouselType;