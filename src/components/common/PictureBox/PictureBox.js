import React from "react";
import cx from "classnames";

import { IconPlusCircle, IconImgDel } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./PictureBox.scss";

const PictureBox = ({
  isTemplate = false,
  pictureInfo,
  onAddClick,
  onDelete,
  ...rest
}) => {
  return (
    <div className="picture-box">
      <div
        className={cx(
          "picture-box-inner",
          isTemplate && "template",
          pictureInfo && "view-picture"
        )}
      >
        {isTemplate && <IconPlusCircle />}
        {pictureInfo && <img src={pictureInfo} alt="picture" />}
      </div>
      {pictureInfo && (
        <div className="black-circle" onClick={onDelete}>
          <IconBase>
            <IconImgDel />
          </IconBase>
        </div>
      )}
    </div>
  );
};

export default PictureBox;
