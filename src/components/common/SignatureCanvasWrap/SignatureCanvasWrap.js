import React, {useState} from 'react';
import SignaturePad from 'react-signature-canvas'
import './SignatureCanvasWrap.scss'
import Button from "../Button";
const SignatureCanvasWrap = () => {
  const [isSigPad, setSigPad] = useState();
  return (
    <>
      <div className={"signature-canvas-wrap"}>
        <SignaturePad
          penColor="black"
          canvasProps={{className: 'sigPad'}}
        />
      </div>
      <Button
        outline
        color={"primary"}
      >삭제</Button>
    </>
  );
};

export default SignatureCanvasWrap;