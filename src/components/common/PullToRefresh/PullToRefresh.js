import React, { useState, useEffect } from "react";
import cx from "classnames";

import GearSpinner from "components/common/GearSpinner";

import "./PullToRefresh.scss";

const THRESHOLD_DISTANCE = 65;
const MAX_DISTANCE = 100;

const PullToRefresh = ({ onRefresh, children }) => {
  const [startY, setStartY] = useState(0);
  const [movedY, setMovedY] = useState(0);
  const [distance, setDistance] = useState(0);
  const [isPull, setIsPull] = useState(false);

  const [refresh, setRefresh] = useState(false);

  const initializePullStyle = () => {
    setStartY(0);
    setMovedY(0);
    setDistance(0);
    setRefresh(false);
  };

  const onTouchStart = (e) => {
    if (e.touches && e.touches.length) {
      setStartY(e.touches[0].clientY);
      setIsPull(true);
    }
  };
  const onTouchMove = (e) => {
    if (e.changedTouches && e.changedTouches.length) {
      setMovedY(e.changedTouches[0].clientY);
    }
  };
  const onTouchEnd = async (e) => {
    if (e.changedTouches && e.changedTouches.length) {
      setIsPull(false);

      setDistance(THRESHOLD_DISTANCE);
      setRefresh(true);
      //   1. async await
      await onRefresh();
      initializePullStyle();
      //   2. promise then
      //   onRefresh().then(initializePullStyle).catch(initializePullStyle);
    }
  };

  useEffect(() => {
    if (isPull) {
      if (movedY - startY < MAX_DISTANCE && movedY - startY > 0) {
        setDistance(movedY - startY);
      } else if (movedY - startY >= MAX_DISTANCE) {
        setRefresh(true);
      }
    }
  }, [isPull, startY, movedY]);

  useEffect(() => {
    initializePullStyle();

    window.addEventListener("mousedown", onTouchStart);
    window.addEventListener("touchstart", onTouchStart);
    window.addEventListener("mousemove", onTouchMove);
    window.addEventListener("touchmove", onTouchMove);
    window.addEventListener("mouseup", onTouchEnd);
    window.addEventListener("touchend", onTouchEnd);
    return () => {
      window.removeEventListener("mousedown", onTouchStart);
      window.removeEventListener("touchstart", onTouchStart);
      window.removeEventListener("mousemove", onTouchMove);
      window.removeEventListener("touchmove", onTouchMove);
      window.removeEventListener("mouseup", onTouchEnd);
      window.removeEventListener("touchend", onTouchEnd);
    };
  }, []);

  return (
    <div className="pull-to-refresh">
      {refresh && (
        <div className="pull-to-refresh-hidden">
          <GearSpinner />
        </div>
      )}
      <div
        className={cx("pull-to-refresh-inner", !isPull && "smooth")}
        style={{ top: distance }}
      >
        {children}
      </div>
    </div>
  );
};

export default PullToRefresh;
