import React, { useState } from "react";
import Lottie from "react-lottie";
import Spinner from "asset/lottie/spinner.json";

const lottieOptions = {
  animationData: Spinner,
  loop: true,
  autoplay: true,
  rendererSettings: {
    className: "add-class",
    preserveAspectRatio: "xMidYMid slice",
  },
};

const SpinnerComponent = () => {
  return (
    <Lottie
      options={lottieOptions}
      isClickToPauseDisabled={false}
      style={{ width: "60px", height: "60px" }}
      eventListeners={[
        {
          eventName: "complete",
          callback: () => console.log("the animation completed"),
        },
      ]}
    />
  );
};

export default SpinnerComponent;
