import React from "react";
import Lottie from "react-lottie";
import gearSpinner from "asset/lottie/gearSpinner.json";
import './GearSpinner.scss'
const lottieOptions = {
  animationData: gearSpinner,
  loop: true,
  autoplay: true,
  rendererSettings: {
    className: "add-class",
    preserveAspectRatio: "xMidYMid slice",
  },
};

const GearSpinner = () => {
  return (
    <div className="loader-gear-spinner">
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "67px", height: "45px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      />
    </div>
  );
};

export default GearSpinner;
