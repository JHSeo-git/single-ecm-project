import React, { useState } from "react";
import Lottie from "react-lottie";
import Loader from "asset/lottie/loader.json";
import styled from "styled-components";

const LottieFix = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const lottieOptions = {
  animationData: Loader,
  loop: true,
  autoplay: true,
  rendererSettings: {
    className: "add-class",
    preserveAspectRatio: "xMidYMid slice",
  },
};

const LoaderComponent = ({ style }) => {
  return (
    <LottieFix style={style}>
      <Lottie
        options={lottieOptions}
        isClickToPauseDisabled={false}
        style={{ width: "58px", height: "58px" }}
        eventListeners={[
          {
            eventName: "complete",
            callback: () => console.log("the animation completed"),
          },
        ]}
      />
    </LottieFix>
  );
};

export default LoaderComponent;
