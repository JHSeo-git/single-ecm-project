import React from "react";
import classNames from "classnames";

import "./ContentsBox.scss";

const ContentsBox = ({ bgType, position, children, style, tabType }) => {
  return (
    <div className={classNames("contents", bgType, position, tabType)} style={style}>
      {children}
    </div>
  );
};

export default ContentsBox;
