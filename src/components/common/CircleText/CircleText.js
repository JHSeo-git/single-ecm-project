import React from "react";

import "./CircleText.scss";

const CircleText = ({ text, ...rest }) => {
  return (
    <div className="count-circle" {...rest}>
      <div className="circle-text">{text}</div>
    </div>
  );
};

export default CircleText;
