import React from "react";

import TimePicker from "rc-time-picker";

import { IconInputClock } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import Button from "components/common/Button";

import "rc-time-picker/assets/index.css";
import "./DateTimePicker.scss";

const DateTimePicker = ({
  timeValue,
  setTimeValue,
  type = "From",
  ...rest
}) => {
  const onChange = (value) => {
    setTimeValue(value);
  };

  const AddOnComponent = (panel) => {
    const onConfirm = () => {
      panel.close();
    };
    return (
      <>
        <div className="button-container">
          <Button size="medium" color="gray" fullWidth onClick={onConfirm}>
            확인
          </Button>
        </div>
        <div className="time-picker-type-absolute">
          <span>{type}</span>
        </div>
      </>
    );
  };

  return (
    <div className="date-time-picker" {...rest}>
      <TimePicker
        value={timeValue}
        onChange={onChange}
        showSecond={false}
        hideDisabledOptions
        use24Hours
        popupClassName="time-picker-inputbox"
        popupStyle={{}}
        inputIcon={
          <IconBase className="input-button-icon">
            <IconInputClock width="14.5" height="14.5" />
          </IconBase>
        }
        clearIcon={<div />}
        addon={AddOnComponent}
        inputReadOnly={true}
      />
    </div>
  );
};

export default DateTimePicker;
