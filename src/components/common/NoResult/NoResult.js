import React from "react";

import "./NoResult.scss";
import EmptyCommon from "../EmptyCommon";


const NoResult = ({title1,subTitle1, subTitle2}) => {
  return (
    <div className="no-result">
      <EmptyCommon />

      <div className="empty-txt">
        <span>{title1}</span>
        <span>
          {subTitle1}<br />
          {subTitle2}
        </span>
      </div>
    </div>
  );
};

export default NoResult;
