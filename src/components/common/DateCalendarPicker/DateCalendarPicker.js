import React, { useState, useEffect } from "react";

import DatePicker, { registerLocale } from "react-datepicker";

import DimedModal from "components/base/DimedModal";

import Button from "components/common/Button";

import {
  IconCalendarCancel,
  IconArrowDown,
  IconArrowLeft,
  IconArrowRight,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { getYear, getMonth, format } from "date-fns";
import ko from "date-fns/locale/ko";

import "react-datepicker/dist/react-datepicker.css";
import "./DateCalendarPicker.scss";

registerLocale("ko", ko);

const DateCalendarPicker = ({
  fromToType,
  fromDate,
  setFromDate,
  toDate,
  setToDate,
  onClose,
}) => {
  /*
    TODO:
      - locale
      - after select period, handle year-month selection
  */

  const [currentFromDt, setCurrentFromDt] = useState(null);
  const [currentToDt, setCurrentToDt] = useState(null);

  const [selectYearMonth, setSelectYearMonth] = useState(new Date());

  const [isSelectYearMonth, setIsSelectYearMonth] = useState(false);

  const onTodayClick = (changeYear, changeMonth) => {
    if (changeYear && changeMonth) {
      changeYear(getYear(new Date()));
      changeMonth(getMonth(new Date()));

      if (isSelectYearMonth) {
        setSelectYearMonth(new Date());
      }
    }
  };

  const onChange = (dates) => {
    if (isSelectYearMonth) return;
    if (fromToType.toUpperCase() === "TO") return;
    const [start, end] = dates;
    setCurrentFromDt(start);
    setCurrentToDt(end);
  };

  const onSelect = (date) => {
    if (isSelectYearMonth) {
      const targetDate = new Date(getYear(date), getMonth(new Date()));
      console.log(targetDate);
      setSelectYearMonth(targetDate);
      return;
    }
    if (fromToType.toUpperCase() === "FROM" && currentToDt) {
      setCurrentFromDt(date);
      return;
    } else if (fromToType.toUpperCase() === "FROM") {
      return;
    }
    setCurrentToDt(date);
  };

  const onConfirm = () => {
    if (isSelectYearMonth) {
      setIsSelectYearMonth(false);
    } else {
      if (currentFromDt) setFromDate(currentFromDt);
      if (currentToDt) setToDate(currentToDt);

      onClose();
    }
  };

  const onCancel = () => {
    onClose();
  };

  const iniitialize = () => {
    // date
    setCurrentFromDt(fromDate);
    setCurrentToDt(toDate);
  };

  useEffect(() => {
    iniitialize();
  }, []);

  return (
    <DimedModal>
      <DatePicker
        renderDayContents={(day) => {
          return <div className="day-circle">{day}</div>;
        }}
        renderCustomHeader={({
          date,
          decreaseMonth,
          increaseMonth,
          changeYear,
          changeMonth,
        }) => (
          <div className="date-calendar-picker-header">
            <div className="header-row">
              <div className="left">
                <IconBase onClick={onCancel}>
                  <IconCalendarCancel />
                </IconBase>
              </div>
              <div className="center">
                <span className="from">
                  {currentFromDt ? format(currentFromDt, "yyyy.MM.dd") : "From"}
                </span>
                <span> - </span>
                <span className="to">
                  {currentToDt ? format(currentToDt, "yyyy.MM.dd") : "To"}
                </span>
              </div>
              <div className="right">
                <button
                  className="btn-today"
                  onClick={() => onTodayClick(changeYear, changeMonth)}
                >
                  <span>오늘</span>
                </button>
              </div>
            </div>
            <div className="header-row">
              <button
                className="btn-select-cal"
                onClick={() => setIsSelectYearMonth(!isSelectYearMonth)}
              >
                <span>{`${getYear(date)}년 ${getMonth(date) + 1}월`}</span>
                <IconBase>
                  <IconArrowDown />
                </IconBase>
              </button>
              <div className="month-move-icons">
                {!isSelectYearMonth && (
                  <>
                    <IconBase className="move-icon" onClick={decreaseMonth}>
                      <IconArrowLeft />
                    </IconBase>
                    <IconBase className="move-icon" onClick={increaseMonth}>
                      <IconArrowRight />
                    </IconBase>
                  </>
                )}
              </div>
            </div>
          </div>
        )}
        useWeekdaysShort={true}
        dateFormat="yyyy.MM.dd(eee)"
        shouldCloseOnSelect={false}
        // todayButton={
        //   <button className="btn-today" onClick={onTodayClick}>
        //     <span>오늘</span>
        //   </button>
        // }
        selected={
          isSelectYearMonth
            ? selectYearMonth
            : fromToType === "FROM"
            ? currentToDt
              ? currentToDt
              : currentFromDt
            : currentToDt
        }
        onSelect={onSelect}
        onChange={onChange}
        startDate={currentFromDt}
        endDate={currentToDt}
        selectsRange
        showYearPicker={isSelectYearMonth}
        yearItemNumber={15}
        inline
      >
        <div className="button-container">
          <Button size="medium" color="gray" fullWidth onClick={onConfirm}>
            확인
          </Button>
        </div>
      </DatePicker>
    </DimedModal>
  );
};

export default DateCalendarPicker;
