import React from 'react';
import { PieChart } from "react-minimal-pie-chart";
import './PieChartCommon.scss'
const dataMock = [
  { title: 'One', value: 23, color: '#77A5FF' },
  { title: 'Two', value: 15, color: '#FFBF6B' },
];

const valueSum = dataMock[0].value + dataMock[1].value;

const PieChartCommon = ({total}) => {
  return (
    <div className="pie-chart-wrap">
      <PieChart
        data={dataMock}
        lineWidth={20}
        paddingAngle={18}
        rounded
        animate
        animationDuration={800}
        animationEasing="ease-out"
        center={[50, 50]}
        label={({ dataEntry }) => dataEntry.value}
        labelStyle={(index) => ({
          fill: dataMock[index].color,
          fontSize: '0',
          fontFamily: 'sans-serif',
        })}
        totalValue={total}
        startAngle={-90}
      />

      <div className="pie-total">
        <span>{valueSum}</span><span>&#47;{total}</span>
      </div>
    </div>
  );
};

export default PieChartCommon;