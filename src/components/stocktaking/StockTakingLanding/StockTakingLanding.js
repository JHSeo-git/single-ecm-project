import React, { useState } from "react";
import cx from "classnames";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";
import StockTakingCardPanel from "components/stocktaking/StockTakingCardPanel";
import StockTakingDetail from "components/stocktaking/StockTakingDetail";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import { IconHamburger } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import AppNavigation from "components/common/AppNavigation";

import "./StockTakingLanding.scss";

const StockTakingLanding = ({ onSearchClick }) => {
  const [mode, setMode] = useState("init");
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const [results] = useState([
    {
      id: "1",
      no: "1156",
      department: "ZMNT01",
      range: "All",
      status: "Draft",
      description: "재고실사테스트 Desc",
      startDate: "2020.12.10",
      endDate: "2020.12.12",
      username: "박선임",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
    {
      id: "2",
      no: "1157",
      department: "ZMNT02",
      range: "All",
      status: "Draft",
      description: "테스트테스트테스트테스트 Desc",
      startDate: "2020.12.12",
      endDate: "2020.12.14",
      username: "홍길동",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
    {
      id: "3",
      no: "1158",
      department: "ZMNT03",
      range: "All",
      status: "ready",
      description: "테스트테스트테스트테스트 Desc",
      startDate: "2020.12.14",
      endDate: "2020.12.15",
      username: "김사원",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
    {
      id: "4",
      no: "1159",
      department: "ZMNT01",
      range: "All",
      status: "ready",
      description: "테스트테스트테스트테스트 Desc",
      startDate: "2020.12.15",
      endDate: "2020.12.17",
      username: "홍사원",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
    {
      id: "5",
      no: "1160",
      department: "ZMNT02",
      range: "All",
      status: "ready",
      description: "테스트테스트테스트테스트 Desc",
      startDate: "2020.12.18",
      endDate: "2020.12.19",
      username: "박사원",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
    {
      id: "6",
      no: "1161",
      department: "ZMNT03",
      range: "All",
      status: "ready",
      description: "테스트테스트테스트테스트 Desc",
      startDate: "2020.12.19",
      endDate: "2020.12.21",
      username: "최사원",
      stockItems: [
        {
          stockId: "BMBOLT0001",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "3",
          gapType: "g1",
          comments: "",
        },
        {
          stockId: "BMBOLT0002",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "4",
          gapType: "g2",
          comments: "",
        },
        {
          stockId: "BMBOLT0003",
          stockDesc:
            "재고실사테스트 Desc재고실사테스트 Desc재고실사테스트 Desc",
          location: "central",
          quantity: "000",
          quantityAvailable: "5",
          gapType: "g3",
          comments: "",
        },
      ],
    },
  ]);

  const [keywords, setKeywords] = useState([
    { id: "1", type: "person", text: "홍길동" },
    { id: "2", type: "part", text: "P7 C/F 일상보전반" },
    { id: "3", type: "fromto", text: "2020-08-11 ~ 2020-08-11" },
    { id: "4", type: "itemCode", text: "123444" },
    { id: "5", type: "itemCode", text: "456555" },
    // { id: "5", type: "maker", text: "aaa" },
    // { id: "6", type: "maker", text: "bbb" },
  ]);

  const [selectedId, setSelectedId] = useState(null);

  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <div className="stock-taking-landing">
          <FixedHeader
            left={
              <IconBase
                onClick={
                  isMenuOpen ? undefined : () => setIsMenuOpen(!isMenuOpen)
                }
              >
                {!isMenuOpen && <IconHamburger />}
              </IconBase>
            }
            center={
              <div className="equipment-title">
                <HeaderTitle text="Stocktaking">
                  {results && <CircleText text={results.length} />}
                </HeaderTitle>
              </div>
            }
          />
          <BaseMain
            className="delivery-main"
            isMenuOpen={isMenuOpen}
            onMenuClick={() => setIsMenuOpen(!isMenuOpen)}
            style={{
              height: `calc(100vh - (56px + 60px + 55px))`,
              marginTop: `calc(56px + 55px)`,
            }}
          >
            <FixedWrapper style={{ top: "56px" }}>
              <SearchConditionPanelTypeB
                onSearchClick={onSearchClick}
                keywords={keywords}
                setKeywords={setKeywords}
                isBorderBot={false}
              />
            </FixedWrapper>
            {results && (
              <ul className="stock-items">
                {results.map((item) => (
                  <li key={item.id} className="stock-item">
                    <StockTakingCardPanel
                      {...item}
                      onClick={() => {
                        setSelectedId(item.id);
                        setMode("STOCKTAKING_DETAIL");
                      }}
                    />
                  </li>
                ))}
              </ul>
            )}
          </BaseMain>
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        </div>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="STOCKTAKING_DETAIL">
        <StockTakingDetail
          items={results.find((result) => result.id === selectedId)?.stockItems}
          status={results.find((result) => result.id === selectedId)?.status}
          onBackClick={() => setMode("")}
        />
      </ModalFullPageAni>
    </>
  );
};

export default StockTakingLanding;
