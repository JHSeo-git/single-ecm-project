import React from "react";
import cx from "classnames";

import IconBase from "components/common/IconBase";
import { IconUnion, IconCalendar } from "components/common/Icons";

import "./StockTakingCardPanel.scss";

const StockTakingCardPanel = ({
  onClick,
  id,
  no,
  department,
  range,
  status,
  description,
  startDate,
  endDate,
  username,
}) => {
  const randomNum = (no % 6) + 1;
  return (
    <div className="stock-taking-card-panel" onClick={onClick}>
      <div className="row spb" style={{ marginBottom: "2px" }}>
        <div className="col">
          <span className="no">{no}</span>
          <div className="department">
            <IconBase className="union-icon">
              <IconUnion width="16" height="16" />
            </IconBase>
            <span className="text">{department}</span>
          </div>
        </div>
        <div className="col">
          <div className="box-text">{range}</div>
          <div className="box-text">{status}</div>
        </div>
      </div>
      <div className="row" style={{ marginBottom: "8px" }}>
        <span className="desc">{description}</span>
      </div>
      <div className="row spb">
        <div className="col">
          <IconBase className="cal-icon">
            <IconCalendar width="24" height="24" />
          </IconBase>
          <span className="date">{`${startDate} - ${endDate}`}</span>
        </div>
        <div className="col">
          <div className={cx("user-circle-name", `bgcolor-${randomNum}`)}>
            {username.substr(0, 1)}
          </div>
          <div className="user-name">{username}</div>
        </div>
      </div>
    </div>
  );
};

export default StockTakingCardPanel;
