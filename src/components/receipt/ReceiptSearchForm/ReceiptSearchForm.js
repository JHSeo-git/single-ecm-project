import React, { useState } from "react";
import { format } from "date-fns";

import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";
import BaseFormMain from "components/base/BaseFormMain";

import DateCalendarPicker from "components/common/DateCalendarPicker";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import CircleWithIcon from "components/common/CircleWithIcon";

import LabelInputTypeB from "components/search/LabelInputTypeB";
import LabelInputTypeE from "components/search/LabelInputTypeE";

import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";
import SearchButtonPanelTypeA from "components/search/SearchButtonPanelTypeA";
import SelectInputDiv from "components/search/SelectInputDiv";
import DimedModal from "components/base/DimedModal";

import LabelInputCircleRadio from "components/workOrder/LabelInputCircleRadio";

import {
  IconCommBack,
  IconInputClock,
  IconCalendar,
  IconDelete12,
  IconRefresh2,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import HeaderTitle from "components/common/HeaderTitle";

import "./ReceiptSearchForm.scss";

const ReceiptSearchForm = ({ onBackClick }) => {
  const [mode, setMode] = useState("init");
  const [subMode, setSubMode] = useState("");
  const onInnerClose = () => {
    setMode("");
  };

  const [keywords, setKeywords] = useState([
    {
      id: "1",
      type: "vendor",
      text: "Vendor1",
      subText: "Vendor Vendor",
    },
    {
      id: "s1",
      type: "status",
      text: "Accept",
    },
  ]);

  const [statusSelectedId, setStatusSelectedId] = useState("s1");

  const onInputAllClearClick = () => {
    setKeywords([]);
    setStatusSelectedId(null);
  };

  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);

  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };

  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [searchExpandMode, setSearchExpandMode] = useState(false);
  const onItemClick = () => {
    setFormType("vendor");
    setMode("FORM_DETAIL");
    setSearchInitValue("");
    setSearchExpandMode(false);
  };

  const receiptType = [
    {
      id: "new",
      value: "NEW",
    },
    {
      id: "reb",
      value: "REB",
    },
  ];

  const [selectedId, setSelectedId] = useState("new");

  const [orderDateFromDt, setOrderDateFromDt] = useState("");
  const [orderDateToDt, setOrderDateToDt] = useState("");
  const [fromToType, setFromToType] = useState("FROM");

  const [deliveryDateFromDt, setDeliveryDateFromDt] = useState("");
  const [deliveryDateToDt, setDeliveryDateToDt] = useState("");
  const [deliveryDatefromToType, setDeliveryDateFromToType] = useState("FROM");

  const onOrderDateCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setFromToType(fromtotype);
    //setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
    setSubMode("DATEPICKER/ORDER_DATE");
  };

  const onDeliveryDateCalendarModal = (e) => {
    e.preventDefault();
    const { currentTarget } = e;
    const {
      dataset: { fromtotype },
    } = currentTarget;

    setDeliveryDateFromToType(fromtotype);
    //setIsShowFirstCalendarBox(!isShowFirstCalenderBox);
    setSubMode("DATEPICKER/DELIVERY_DATE");
  };

  return (
    <>
      <ModalFullPageAni
        className="receipt-search-form"
        modeState={mode}
        isBaseComponent={true}
        pageName=""
      >
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack />
            </IconBase>
          }
          center={<HeaderTitle text="Search" />}
        />
        <BaseFormMain
          style={{
            height: `calc(100vh - (56px + ${isKeyboardFocus ? `0px` : `79px`})`,
            marginTop: `56px`,
          }}
        >
          <div className="label-input">
            <LabelInputTypeB
              labelText="Inbound Delivery No"
              onToggleFocus={onToggleFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeE
              labelText="Vendor"
              keywords={keywords}
              setKeywords={setKeywords}
              searchType="itemCode"
              onInputClick={onItemClick}
              setKeyboardFocus={setIsKeyboardFocus}
            />
          </div>
          <div className="label-input">
            <LabelInputTypeB labelText="PO No" onToggleFocus={onToggleFocus} />
          </div>
          <div className="label-input">
            <LabelInputCircleRadio
              labelText="Receipt Type"
              items={receiptType}
              radioGroupName="receiptType"
              selectedId={selectedId}
              setSelectedId={setSelectedId}
            />
          </div>
          <div className="label-input">
            <div className="label">Order Date</div>
            <div className="date-input-row">
              <div className="date-input-rowcol">
                <div className="calendar-fromto-type">
                  <div className="label">From</div>
                  <div
                    className="date-calendar-input input-col"
                    onClick={onOrderDateCalendarModal}
                    data-fromtotype="FROM"
                  >
                    <IconBase>
                      <IconCalendar width="17" height="18" />
                    </IconBase>
                    <input
                      type="text"
                      value={
                        orderDateFromDt && format(orderDateFromDt, "yyyy.MM.dd")
                      }
                      placeholder="From"
                      readOnly
                    />
                  </div>
                  <IconBase
                    className="icon-reset-alt"
                    onClick={() => {
                      setOrderDateFromDt("");
                    }}
                  >
                    <IconRefresh2 />
                  </IconBase>
                </div>
                <div className="calendar-fromto-type">
                  <div className="label">To</div>
                  <div
                    className="date-calendar-input input-col"
                    onClick={onOrderDateCalendarModal}
                    data-fromtotype="TO"
                  >
                    <IconBase>
                      <IconCalendar width="17" height="18" />
                    </IconBase>
                    <input
                      type="text"
                      value={
                        orderDateToDt && format(orderDateToDt, "yyyy.MM.dd")
                      }
                      placeholder="To"
                      readOnly
                    />
                  </div>
                  <IconBase
                    className="icon-reset-alt"
                    onClick={() => {
                      setOrderDateToDt("");
                    }}
                  >
                    <IconRefresh2 />
                  </IconBase>
                </div>
              </div>
            </div>
          </div>
          <div className="label-input">
            <div className="label">Delivery Date</div>
            <div className="date-input-row">
              <div className="date-input-rowcol">
                <div className="calendar-fromto-type">
                  <div className="label">From</div>
                  <div
                    className="date-calendar-input input-col"
                    onClick={onDeliveryDateCalendarModal}
                    data-fromtotype="FROM"
                  >
                    <IconBase>
                      <IconCalendar width="17" height="18" />
                    </IconBase>
                    <input
                      type="text"
                      value={
                        deliveryDateFromDt &&
                        format(deliveryDateFromDt, "yyyy.MM.dd")
                      }
                      placeholder="From"
                      readOnly
                    />
                  </div>
                  <IconBase
                    className="icon-reset-alt"
                    onClick={() => {
                      setDeliveryDateFromDt("");
                    }}
                  >
                    <IconRefresh2 />
                  </IconBase>
                </div>
                <div className="calendar-fromto-type">
                  <div className="label">To</div>
                  <div
                    className="date-calendar-input input-col"
                    onClick={onDeliveryDateCalendarModal}
                    data-fromtotype="TO"
                  >
                    <IconBase>
                      <IconCalendar width="17" height="18" />
                    </IconBase>
                    <input
                      type="text"
                      value={
                        deliveryDateToDt &&
                        format(deliveryDateToDt, "yyyy.MM.dd")
                      }
                      placeholder="To"
                      readOnly
                    />
                  </div>
                  <IconBase
                    className="icon-reset-alt"
                    onClick={() => {
                      setDeliveryDateToDt("");
                    }}
                  >
                    <IconRefresh2 />
                  </IconBase>
                </div>
              </div>
            </div>
          </div>
        </BaseFormMain>
        {!isKeyboardFocus && (
          <FixedFooter>
            <SearchButtonPanelTypeA
              onInputAllClearClick={onInputAllClearClick}
              clearNumber={keywords?.length > 0 && keywords?.length}
              btnColor="gray"
              btnText="검색"
            />
          </FixedFooter>
        )}
        {subMode === "DATEPICKER/ORDER_DATE" && (
          <DateCalendarPicker
            fromToType={fromToType}
            fromDate={orderDateFromDt}
            setFromDate={setOrderDateFromDt}
            toDate={orderDateToDt}
            setToDate={setOrderDateToDt}
            onClose={() => setSubMode("")}
          />
        )}
        {subMode === "DATEPICKER/DELIVERY_DATE" && (
          <DateCalendarPicker
            fromToType={deliveryDatefromToType}
            fromDate={deliveryDateFromDt}
            setFromDate={setDeliveryDateFromDt}
            toDate={deliveryDateToDt}
            setToDate={setDeliveryDateToDt}
            onClose={() => setSubMode("")}
          />
        )}
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={onInnerClose}
          initValue={searchInitValue}
          isExpandMode={searchExpandMode}
        />
      </ModalFullPageAni>
    </>
  );
};

export default ReceiptSearchForm;
