import React from "react";

import { IconDeliveryTabRequest } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./ReceiptCardPanel.scss";

const ReceiptCardPanel = () => {
  return (
    <div className="receipt-card-panel">
      <div className="card-row spb" style={{ marginBottom: "4px" }}>
        <div className="card-title">LGES_Inbound_210126_001</div>
        <div className="card-dttm">2020.05.01</div>
      </div>
      <div className="card-row" style={{ marginBottom: "8px" }}>
        <div className="card-col" style={{ marginRight: "60px" }}>
          <div className="card-label">PO No.</div>
          <div className="card-value">1181</div>
        </div>
        <div className="card-col">
          <div className="card-label">PO Line.</div>
          <div className="card-value">1</div>
        </div>
      </div>
      <div className="card-row">
        <div className="circle-text">
          <div className="card-desc-label">Vendor.</div>
          <div className="card-desc-value">ALMAR</div>
        </div>
        <div className="circle-text">
          <div className="card-desc-label">Item.</div>
          <div className="card-desc-value">CCAA0001</div>
        </div>
        <div className="circle-text">
          <IconBase className="receipt-icon">
            <IconDeliveryTabRequest width="18" height="18" />
          </IconBase>
          <div className="card-desc-value">1</div>
        </div>
      </div>
    </div>
  );
};

export default ReceiptCardPanel;
