import React, { useState } from "react";
import cx from "classnames";

import ModalFullPageAni from "components/common/ModalFullPageAni";
import FixedHeader from "components/base/FixedHeader";
import FixedWrapper from "components/base/FixedWrapper";
import BaseMain from "components/base/BaseMain";

import CircleText from "components/common/CircleText";
import HeaderTitle from "components/common/HeaderTitle";

import SearchConditionPanelTypeB from "components/search/SearchConditionPanelTypeB";

import {
  IconHamburger,
  IconBarcodeButton,
  IconMainReleaseFull,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import FixedFooter from "components/base/FixedFooter";
import ReceiptCardPanel from "components/receipt/ReceiptCardPanel";
import ReceiptBarcode from "components/receipt/ReceiptBarcode";
import ReceiptDetail from "components/receipt/ReceiptDetail";

import { useSnackbar } from "notistack";

import "./ReceiptLanding.scss";

const ReceiptLanding = ({ onSearchClick }) => {
  const [mode, setMode] = useState("init");
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const [subMode, setSubMode] = useState("");

  const [keywords, setKeywords] = useState([
    { id: "1", type: "itemCode", text: "QCA" },
    { id: "2", type: "fromto", text: "2020-08-11 ~ 2020-08-11" },
  ]);

  const [results] = useState([]);

  const [tabMode, setTabMode] = useState("new");

  const [prevScrollTop, setPrevScrollTop] = useState(0);
  const [hide, setHide] = useState(false);
  const handleScroll = (e) => {
    const scrollTop = e.currentTarget.scrollTop;
    const deltaY = scrollTop - prevScrollTop;
    const tmpHide = scrollTop !== 0 && deltaY >= 0;
    setHide(tmpHide);
    setPrevScrollTop(scrollTop);
  };

  const [selectedId, setSelectedId] = useState("");

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (inMessage, isOK = true) => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar(inMessage, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: 108 }}
          onClick={() => closeSnackbar(key)}
        >
          <div className="ok-wrapper">
            {isOK && (
              <IconMainReleaseFull fill="#04B395" width="22" height="22" />
            )}
            <span className="text">{message}</span>
          </div>
        </div>
      ),
    });
  };
  return (
    <>
      <ModalFullPageAni isBaseComponent={true} modeState={mode} pageName="">
        <div className="receipt-landing">
          <FixedHeader
            left={
              <IconBase
                onClick={
                  isMenuOpen ? undefined : () => setIsMenuOpen(!isMenuOpen)
                }
              >
                {!isMenuOpen && <IconHamburger />}
              </IconBase>
            }
            center={
              <div className="equipment-title">
                <HeaderTitle text="Receipt">
                  {results && <CircleText text={results.length} />}
                </HeaderTitle>
              </div>
            }
          />
          <BaseMain
            className="receipt-main"
            isMenuOpen={isMenuOpen}
            onMenuClick={() => setIsMenuOpen(!isMenuOpen)}
            style={{
              height: `calc(100vh - (56px + 55px))`,
              marginTop: `calc(56px + 55px)`,
            }}
            onScroll={handleScroll}
          >
            <FixedWrapper style={{ top: "56px" }}>
              <SearchConditionPanelTypeB
                onSearchClick={onSearchClick}
                keywords={keywords}
                setKeywords={setKeywords}
                isBorderBot={false}
              />
            </FixedWrapper>
            <div className={cx("receipt-header", !hide && "sticky-header")}>
              <div className="inner">
                <div
                  className={cx("receipt-tab", tabMode === "new" && "active")}
                  onClick={() => setTabMode("new")}
                >
                  <span className="tab-text">NEW</span>
                </div>
                <div
                  className={cx("receipt-tab", tabMode === "reb" && "active")}
                  onClick={() => setTabMode("reb")}
                >
                  <span className="tab-text">REB</span>
                </div>
                <div className={cx("animation-bg", tabMode)} />
              </div>
            </div>
            {tabMode === "new" && (
              <ul
                className="receipt-items"
                style={{ marginTop: `calc(14px + ${!hide ? "56px" : "0px"})` }}
              >
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/NEW");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/NEW");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/NEW");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/NEW");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/NEW");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
              </ul>
            )}
            {tabMode === "reb" && (
              <ul
                className="receipt-items"
                style={{ marginTop: `calc(14px + ${!hide ? "56px" : "0px"})` }}
              >
                <li
                  className="receipt-item"
                  onClick={() => {
                    setMode("DETAIL/REB");
                    setSelectedId("test01");
                  }}
                >
                  <ReceiptCardPanel />
                </li>
              </ul>
            )}
          </BaseMain>
          <FixedFooter>
            <div className="barcode-button-wrapper">
              <IconBase
                className="barcode-button"
                onClick={() => setSubMode("BARCODE")}
              >
                <IconBarcodeButton />
              </IconBase>
            </div>
          </FixedFooter>
        </div>
        <ModalFullPageAni
          modeState={subMode}
          pageName="BARCODE"
          motion="vertical"
        >
          <ReceiptBarcode
            onClose={() => setSubMode("")}
            onSuccess={(sId) => {
              setSubMode("");
              setSelectedId(sId);
              if (tabMode === "new") {
                setMode("DETAIL/NEW");
              } else {
                setMode("DETAIL/REB");
              }
            }}
            onShowMessage={onShowMessage}
          />
        </ModalFullPageAni>
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="DETAIL/NEW">
        <ReceiptDetail type="new" onBackClick={() => setMode("")} />
      </ModalFullPageAni>
      <ModalFullPageAni modeState={mode} pageName="DETAIL/REB">
        <ReceiptDetail type="reb" onBackClick={() => setMode("")} />
      </ModalFullPageAni>
    </>
  );
};

export default ReceiptLanding;
