import React, { useState } from "react";
import cx from "classnames";

import FixedHeader from "components/base/FixedHeader";
import LabelInputTypeE from "components/search/LabelInputTypeE";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import FormDetailOnlyOne from "components/search/FormDetailOnlyOne";

import {
  IconCommBack,
  IconMainReleaseFull,
  IconMainAlertFull,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";

import { useSnackbar } from "notistack";

import HeaderTitle from "components/common/HeaderTitle";

import FixedFooter from "components/base/FixedFooter";
import "./ReceiptDetail.scss";

const ReceiptDetail = ({ onBackClick, type = "new" }) => {
  const [mode, setMode] = useState("");
  const [formType, setFormType] = useState("");
  const [searchInitValue, setSearchInitValue] = useState("");
  const [isKeyboardFocus, setIsKeyboardFocus] = useState(false);
  const onToggleFocus = (e) => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains("input-box")) {
      currentTarget.classList.toggle("active");
      setIsKeyboardFocus(!isKeyboardFocus);
    }
  };
  const onItemClick = (e, type, value) => {
    setFormType(type);
    setMode("FORM_DETAIL");
    setSearchInitValue(value);
  };

  const onSubmit = (sId) => {
    onShowMessage(`'${sId}'의 입고신청이 되었습니다.`);
    onBackClick();
  };

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const onShowMessage = (inMessage, isOK = true) => {
    // if (isShowMessageBar) return;
    // setIsShowMessageBar(true);
    // setTimeout(() => setIsShowMessageBar(false), 3500);
    enqueueSnackbar(inMessage, {
      autoHideDuration: 2000,
      content: (key, message) => (
        <div
          className="notistack"
          id={key}
          style={{ bottom: "108px" }}
          onClick={() => closeSnackbar(key)}
        >
          <div className="ok-wrapper">
            {isOK ? (
              <IconMainReleaseFull fill="#04B395" width="22" height="22" />
            ) : (
              <IconMainAlertFull width="22" height="22" />
            )}
            <span className="text">{message}</span>
          </div>
        </div>
      ),
    });
  };
  return (
    <>
      <div className="receipt-detail">
        <FixedHeader
          left={
            <IconBase onClick={onBackClick}>
              <IconCommBack fill="#ffffff" bgFill="#424952" />
            </IconBase>
          }
          center={
            <HeaderTitle
              text={`${type === "new" ? "NEW" : "REB"} Receipt`}
              blackMode={true}
            />
          }
          blackMode={true}
        />

        <main
          className="receipt-detail-main"
          style={{
            height: `calc(100vh - (56px + 90px))`,
            marginTop: `56px`,
            overflowY: `auto`,
          }}
        >
          <div className="sticky-header">
            <div className="header-row flex-spb">
              <div className="date">2020.05.01</div>
              <div className="vendor">ALMAR</div>
            </div>
            <div className="header-row">
              <div className="title">LGES_Inbound_210126_ 001</div>
            </div>
          </div>
          <div className="content-panel">
            <div className="content-row">
              <div className="col">
                <div className="label">PO No</div>
                <div className="value">1181</div>
              </div>
              <div className="col">
                <div className="label">PO Line</div>
                <div className="value">1</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Item Code</div>
                <div className="value">CCAA0001</div>
              </div>
              <div className="col">
                <div className="label">Item Qty</div>
                <div className="value">3</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Item Description</div>
                <div className="value">
                  ELECTRICAL, GGM, INTERNATIONAL LIMITED (H/O)
                  MODELASEDFASDFASDF
                </div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Location</div>
                <div className="value">Central</div>
              </div>
              <div className="col">
                {/* <div className="label">Bin</div>
                <input
                  type="text"
                  className="input-value"
                  placeholder="Placeholder"
                /> */}
                <LabelInputTypeE
                  labelText="Bin"
                  searchType="bin"
                  onInputClick={onItemClick}
                  setKeyboardFocus={setIsKeyboardFocus}
                  expandMode={false}
                />
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Unit Cost</div>
                <div className="value">1,000</div>
              </div>
              <div className="col">
                <div className="label">Line Cost</div>
                <div className="value">3,000</div>
              </div>
            </div>
            <div className="content-row">
              <div className="col">
                <div className="label">Ordered Date</div>
                <div className="value">2021.01.20</div>
              </div>
              <div className="col">
                <div className="label">Condition</div>
                <div className="value">
                  <div className="circle">NEW</div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <FixedFooter className="footer-white">
          <div
            className="receipt-button"
            onClick={() => {
              onSubmit("test01");
            }}
          >
            <span className="button-text">입고</span>
          </div>
        </FixedFooter>
      </div>
      <ModalFullPageAni modeState={mode} pageName="FORM_DETAIL">
        <FormDetailOnlyOne
          type={formType}
          onClose={() => setMode("")}
          initValue={searchInitValue}
          isExpandMode={true}
          externalMode="FLEX_MODE"
        />
      </ModalFullPageAni>
    </>
  );
};

export default ReceiptDetail;
