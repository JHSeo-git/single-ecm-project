import React, { useState } from "react";
import cx from "classnames";

import MenuNav from "components/base/MenuNav";
import FixedHeader from "components/base/FixedHeader";
import FixedFooter from "components/base/FixedFooter";

import {
  IconHamburger,
  IconInventory,
  IconWarehousing,
  IconDailyCheck,
  IconRelease,
  IconInventoryCount,
  IconInventoryMovement,
  IconBigNone1,
  IconBigNone2,
  IconBigNone3,
  IconBigNone4,
  IconBigNone5,
} from "components/common/Icons";
import IconBase from "components/common/IconBase";
import AppNavigation from "components/common/AppNavigation";
import HeaderTitle from "components/common/HeaderTitle";
import MainMenuButton from "components/main/MainMenuButton";
import MainUserCard from "components/main/MainUserCard";
import "./Main.scss";

const MainWarehouse = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isHeight, setIsHeight] = useState(false);
  const onMenuClick = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <>
      <div className="main-wrapper">
        <FixedHeader
          left={
            <IconBase onClick={isMenuOpen ? undefined : onMenuClick}>
              {!isMenuOpen && <IconHamburger />}
            </IconBase>
          }
          center={<HeaderTitle text="Home" />}
        />
        <main className={cx("main", isMenuOpen && "modal-open")}>
          {isMenuOpen && <MenuNav onMenuClick={onMenuClick} />}
          <div className="main-user">
            <MainUserCard firstName="홍" lastName="길동" staffRank="사원" />
          </div>
          <div className="main-menu-buttons">
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconWarehousing width="64" height="64" />}
                text="Receipt"
                linkUrl="/work-order-list"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconRelease width="64" height="64" />}
                text="Delivery"
                linkUrl="/search-equipment"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconInventoryCount width="64" height="64" />}
                text="Stocktaking"
                linkUrl="/search-sp-inventory"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconInventoryMovement width="64" height="64" />}
                text="재고이동"
                linkUrl="/search-sp"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconDailyCheck width="64" height="64" />}
                text="Spare Parts"
                linkUrl="/search-sp"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconInventory width="64" height="64" />}
                text="Inventory"
                linkUrl="/search-sp-inventory"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconBigNone1 width="64" height="64" />}
                text="None"
                linkUrl="/search-sp"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconBigNone2 width="64" height="64" />}
                text="None"
                linkUrl="/search-sp"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconBigNone3 width="64" height="64" />}
                text="None"
                linkUrl="/search-sp"
                className="menu-button small"
              />
              <MainMenuButton
                icon={<IconBigNone4 width="64" height="64" />}
                text="None"
                linkUrl="/search-sp"
                className="menu-button small"
              />
            </div>
            <div className="main-menu-row">
              <MainMenuButton
                icon={<IconBigNone5 width="64" height="64" />}
                text="None"
                linkUrl="/search-sp"
                className="menu-button small"
              />
            </div>
          </div>
        </main>
        {!isMenuOpen && (
          <FixedFooter>
            <AppNavigation />
          </FixedFooter>
        )}
      </div>
    </>
  );
};

export default MainWarehouse;
