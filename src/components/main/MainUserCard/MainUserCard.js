import React, { useState } from "react";

import { IconPersonPolygon } from "components/common/Icons";
import IconBase from "components/common/IconBase";
import ModalFullPageAni from "components/common/ModalFullPageAni";
import SettingLanding from "components/setting/SettingLanding";

import "./MainUserCard.scss";

const MainUserCard = ({ firstName, lastName, staffRank }) => {
  const [mode, setMode] = useState("");
  return (
    <>
      <div className="main-user-card">
        <div className="card-col">
          <div className="firstName-circle">
            <span className="firtName">{firstName}</span>
          </div>
        </div>
        <div className="card-col flex-col">
          <span className="main-user-hi">안녕하세요,</span>
          <span className="fullName">
            {`${firstName}${lastName} ${staffRank}님`}
          </span>
        </div>
        <div className="setting">
          <IconBase className="profile-icon" onClick={() => setMode("SETTING")}>
            <IconPersonPolygon width="20" height="20" />
          </IconBase>
        </div>
      </div>
      <ModalFullPageAni modeState={mode} pageName="SETTING">
        <SettingLanding onClose={() => setMode("")} />
      </ModalFullPageAni>
    </>
  );
};

export default MainUserCard;
