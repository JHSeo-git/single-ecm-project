import React from "react";
import { Link } from "react-router-dom";
import cx from "classnames";
import IconBase from "components/common/IconBase";

import "./MainMenuButton.scss";

const MainMenuButton = ({ icon, text, linkUrl, counter, className }) => {
  return (
    <div className="main-menu-col">
      {counter && (
        <div className="counter">{counter > 999 ? "999+" : counter}</div>
      )}
      <Link to={linkUrl} className={cx("main-menu-button", className)}>
        <IconBase>{icon}</IconBase>
        <span className="menu-text">{text}</span>
      </Link>
    </div>
  );
};

export default MainMenuButton;
