import React from "react";
import cx from "classnames";

import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import "./OKCancelModal.scss";

const OKCancelModal = ({
  title,
  className,
  children,
  onConfirm,
  onClose,
  ...rest
}) => {
  return (
    <div className={cx("ok-cancel-modal", className)}>
      <div className="ok-cancel-modal-inner">
        <div className="modal-item modal-item--title">
          <span>{title}</span>
          {onClose && (
            <IconBase onClick={onClose}>
              <IconClosePopup />
            </IconBase>
          )}
        </div>
        <div className="modal-item modal-item--content">{children}</div>
        <div className="modal-item modal-item--btns">
          {onClose && (
            <button className="modal-cancel-btn" onClick={onClose}>
              <span>취소</span>
            </button>
          )}
          <button className="modal-ok-btn" onClick={onConfirm}>
            <span>확인</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default OKCancelModal;
