import React from "react";

import DimedModal from "components/base/DimedModal";
import { IconClosePopup } from "components/common/Icons";
import IconBase from "components/common/IconBase";

import SuccessLottie from "components/common/SuccessLottie";

import "./SuccessModal.scss";

const SuccessModal = ({ content, onClick, onClose }) => {
  return (
    <DimedModal className="modal-top-max">
      <div className="success-modal-inner">
        <div className="success-modal-header">
          <span>Success!</span>
          <IconBase onClick={onClose}>
            <IconClosePopup />
          </IconBase>
        </div>
        <div className="success-modal-content">
          <SuccessLottie />
          <div className="content">{content}</div>
          <div className="confirm-button" onClick={onClick}>
            확인
          </div>
        </div>
      </div>
    </DimedModal>
  );
};

export default SuccessModal;
